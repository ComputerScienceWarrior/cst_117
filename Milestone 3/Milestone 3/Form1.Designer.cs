﻿namespace Milestone_3
{
	partial class InventoryForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.InventoryContents = new System.Windows.Forms.ListBox();
			this.printInventoryButton = new System.Windows.Forms.Button();
			this.clearButton = new System.Windows.Forms.Button();
			this.exitButton = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.restockButton = new System.Windows.Forms.Button();
			this.removeButton = new System.Windows.Forms.Button();
			this.addButton = new System.Windows.Forms.Button();
			this.searchButton = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.searchProductPrice = new System.Windows.Forms.TextBox();
			this.searchProductName = new System.Windows.Forms.TextBox();
			this.addProductVendor = new System.Windows.Forms.TextBox();
			this.addProductQuantity = new System.Windows.Forms.TextBox();
			this.addProductCalories = new System.Windows.Forms.TextBox();
			this.addProductPrice = new System.Windows.Forms.TextBox();
			this.addProductName = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// InventoryContents
			// 
			this.InventoryContents.FormattingEnabled = true;
			this.InventoryContents.ItemHeight = 16;
			this.InventoryContents.Location = new System.Drawing.Point(182, 34);
			this.InventoryContents.Name = "InventoryContents";
			this.InventoryContents.Size = new System.Drawing.Size(225, 180);
			this.InventoryContents.TabIndex = 0;
			this.InventoryContents.SelectedIndexChanged += new System.EventHandler(this.InventoryContents_SelectedIndexChanged);
			// 
			// printInventoryButton
			// 
			this.printInventoryButton.AutoSize = true;
			this.printInventoryButton.Location = new System.Drawing.Point(14, 34);
			this.printInventoryButton.Name = "printInventoryButton";
			this.printInventoryButton.Size = new System.Drawing.Size(126, 27);
			this.printInventoryButton.TabIndex = 1;
			this.printInventoryButton.Text = "Display Inventory";
			this.printInventoryButton.UseVisualStyleBackColor = true;
			this.printInventoryButton.Click += new System.EventHandler(this.printInventoryButton_Click_1);
			// 
			// clearButton
			// 
			this.clearButton.AutoSize = true;
			this.clearButton.Location = new System.Drawing.Point(182, 234);
			this.clearButton.Name = "clearButton";
			this.clearButton.Size = new System.Drawing.Size(75, 27);
			this.clearButton.TabIndex = 2;
			this.clearButton.Text = "Clear";
			this.clearButton.UseVisualStyleBackColor = true;
			this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
			// 
			// exitButton
			// 
			this.exitButton.AutoSize = true;
			this.exitButton.Location = new System.Drawing.Point(332, 234);
			this.exitButton.Name = "exitButton";
			this.exitButton.Size = new System.Drawing.Size(75, 27);
			this.exitButton.TabIndex = 3;
			this.exitButton.Text = "Exit";
			this.exitButton.UseVisualStyleBackColor = true;
			this.exitButton.Click += new System.EventHandler(this.exitButton_Click_1);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.printInventoryButton);
			this.groupBox1.Controls.Add(this.exitButton);
			this.groupBox1.Controls.Add(this.clearButton);
			this.groupBox1.Controls.Add(this.restockButton);
			this.groupBox1.Controls.Add(this.InventoryContents);
			this.groupBox1.Controls.Add(this.removeButton);
			this.groupBox1.Location = new System.Drawing.Point(12, 23);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(463, 280);
			this.groupBox1.TabIndex = 4;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "View, Remove or Restock";
			// 
			// restockButton
			// 
			this.restockButton.AutoSize = true;
			this.restockButton.Location = new System.Drawing.Point(14, 123);
			this.restockButton.Name = "restockButton";
			this.restockButton.Size = new System.Drawing.Size(126, 27);
			this.restockButton.TabIndex = 7;
			this.restockButton.Text = "Restock Product";
			this.restockButton.UseVisualStyleBackColor = true;
			// 
			// removeButton
			// 
			this.removeButton.AutoSize = true;
			this.removeButton.Location = new System.Drawing.Point(14, 78);
			this.removeButton.Name = "removeButton";
			this.removeButton.Size = new System.Drawing.Size(126, 27);
			this.removeButton.TabIndex = 6;
			this.removeButton.Text = "Remove Product";
			this.removeButton.UseVisualStyleBackColor = true;
			this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
			// 
			// addButton
			// 
			this.addButton.AutoSize = true;
			this.addButton.Location = new System.Drawing.Point(23, 44);
			this.addButton.Name = "addButton";
			this.addButton.Size = new System.Drawing.Size(126, 27);
			this.addButton.TabIndex = 2;
			this.addButton.Text = "Add Product";
			this.addButton.UseVisualStyleBackColor = true;
			this.addButton.Click += new System.EventHandler(this.addButton_Click);
			// 
			// searchButton
			// 
			this.searchButton.AutoSize = true;
			this.searchButton.Location = new System.Drawing.Point(23, 142);
			this.searchButton.Name = "searchButton";
			this.searchButton.Size = new System.Drawing.Size(126, 27);
			this.searchButton.TabIndex = 8;
			this.searchButton.Text = "Search";
			this.searchButton.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.label1);
			this.groupBox2.Controls.Add(this.searchProductPrice);
			this.groupBox2.Controls.Add(this.searchProductName);
			this.groupBox2.Controls.Add(this.addProductVendor);
			this.groupBox2.Controls.Add(this.addProductQuantity);
			this.groupBox2.Controls.Add(this.addProductCalories);
			this.groupBox2.Controls.Add(this.addProductPrice);
			this.groupBox2.Controls.Add(this.addProductName);
			this.groupBox2.Controls.Add(this.searchButton);
			this.groupBox2.Controls.Add(this.addButton);
			this.groupBox2.Location = new System.Drawing.Point(12, 309);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(778, 228);
			this.groupBox2.TabIndex = 5;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "groupBox2";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(302, 113);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(46, 17);
			this.label7.TabIndex = 20;
			this.label7.Text = "Price";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(179, 113);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(51, 17);
			this.label6.TabIndex = 19;
			this.label6.Text = "Name";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(659, 19);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(65, 17);
			this.label5.TabIndex = 18;
			this.label5.Text = "Vendor";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(541, 19);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(74, 17);
			this.label4.TabIndex = 17;
			this.label4.Text = "Quantity";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(422, 19);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(70, 17);
			this.label3.TabIndex = 16;
			this.label3.Text = "Calories";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(302, 19);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(46, 17);
			this.label2.TabIndex = 15;
			this.label2.Text = "Price";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(179, 19);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(51, 17);
			this.label1.TabIndex = 8;
			this.label1.Text = "Name";
			// 
			// searchProductPrice
			// 
			this.searchProductPrice.Location = new System.Drawing.Point(296, 142);
			this.searchProductPrice.Name = "searchProductPrice";
			this.searchProductPrice.Size = new System.Drawing.Size(100, 22);
			this.searchProductPrice.TabIndex = 14;
			// 
			// searchProductName
			// 
			this.searchProductName.Location = new System.Drawing.Point(173, 142);
			this.searchProductName.Name = "searchProductName";
			this.searchProductName.Size = new System.Drawing.Size(100, 22);
			this.searchProductName.TabIndex = 13;
			// 
			// addProductVendor
			// 
			this.addProductVendor.Location = new System.Drawing.Point(653, 44);
			this.addProductVendor.Name = "addProductVendor";
			this.addProductVendor.Size = new System.Drawing.Size(100, 22);
			this.addProductVendor.TabIndex = 12;
			// 
			// addProductQuantity
			// 
			this.addProductQuantity.Location = new System.Drawing.Point(535, 44);
			this.addProductQuantity.Name = "addProductQuantity";
			this.addProductQuantity.Size = new System.Drawing.Size(100, 22);
			this.addProductQuantity.TabIndex = 11;
			// 
			// addProductCalories
			// 
			this.addProductCalories.Location = new System.Drawing.Point(416, 44);
			this.addProductCalories.Name = "addProductCalories";
			this.addProductCalories.Size = new System.Drawing.Size(100, 22);
			this.addProductCalories.TabIndex = 10;
			// 
			// addProductPrice
			// 
			this.addProductPrice.Location = new System.Drawing.Point(296, 44);
			this.addProductPrice.Name = "addProductPrice";
			this.addProductPrice.Size = new System.Drawing.Size(100, 22);
			this.addProductPrice.TabIndex = 9;
			// 
			// addProductName
			// 
			this.addProductName.Location = new System.Drawing.Point(173, 44);
			this.addProductName.Name = "addProductName";
			this.addProductName.Size = new System.Drawing.Size(100, 22);
			this.addProductName.TabIndex = 8;
			// 
			// InventoryForm
			// 
			this.ClientSize = new System.Drawing.Size(908, 576);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "InventoryForm";
			this.Text = "Remove Product";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox InventoryContents;
		private System.Windows.Forms.Button printInventoryButton;
		private System.Windows.Forms.Button clearButton;
		private System.Windows.Forms.Button exitButton;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button searchButton;
		private System.Windows.Forms.Button restockButton;
		private System.Windows.Forms.Button addButton;
		private System.Windows.Forms.Button removeButton;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox addProductPrice;
		private System.Windows.Forms.TextBox addProductName;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox searchProductPrice;
		private System.Windows.Forms.TextBox searchProductName;
		private System.Windows.Forms.TextBox addProductVendor;
		private System.Windows.Forms.TextBox addProductQuantity;
		private System.Windows.Forms.TextBox addProductCalories;
	}
}

