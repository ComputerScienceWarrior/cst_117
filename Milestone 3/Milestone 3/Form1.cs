﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 * All code in this project is made by James-Ryan Stampley
 * and no one else can take credit for my work.
 * 
 * https://bitbucket.org/ComputerScienceWarrior/cst_117
 */


namespace Milestone_3
{
	public partial class InventoryForm : Form
	{
		//create a field of inventory products 
		InventoryItem[] items = new InventoryItem[3];

		public InventoryForm()
		{
			InitializeComponent();
		}

		private void exitButton_Click_1(object sender, EventArgs e)
		{
			//close program
			this.Close();
		}

		private void clearButton_Click(object sender, EventArgs e)
		{
			//clear contents of inventory items
			InventoryContents.Items.Clear();
		}

		private void printInventoryButton_Click_1(object sender, EventArgs e)
		{
			//clear inventory contents upon each click to prevent duplicates
			InventoryContents.Items.Clear();

			/*
			//create 5 items to add to inventory
			InventoryItem soda = new InventoryItem("Coke");
			InventoryItem chips = new InventoryItem("Plain Lays");
			InventoryItem newspaper = new InventoryItem("Daily Newspaper");
			InventoryItem lighter = new InventoryItem("Red Lighter");
			InventoryItem candybar = new InventoryItem("Snickers");

			//add the items to an array
			items = new InventoryItem[10];
			items[0] = soda;
			items[1] = chips;
			items[2] = newspaper;
			items[3] = lighter;
			items[4] = candybar;
			*/
		

			//loop through created items, and add items to the array
			for (int i = 0; i < items.Length; i++)
			{
				InventoryContents.Items.Add(items[i].printItems()); //method call to display items with printItems method
			}
		}

		private void removeButton_Click(object sender, EventArgs e)
		{

		}

		private void addButton_Click(object sender, EventArgs e)
		{
			InventoryManager inv = new InventoryManager(items);//inventoryManager object to use add method

			
			//create 5 items to add to inventory
			InventoryItem soda = new InventoryItem("Coke");
			InventoryItem chips = new InventoryItem("Plain Lays");
			InventoryItem newspaper = new InventoryItem("Daily Newspaper");
			InventoryItem lighter = new InventoryItem("Red Lighter");
			InventoryItem candybar = new InventoryItem("Snickers");

			//add the items to an array
			items = new InventoryItem[5];

			//items to add to array
			items[0] = soda;
			items[1] = chips;
			items[2] = newspaper;
			items[3] = lighter;
			items[4] = candybar;

			inv.add(items);//items added using add method
			
			
			//adds item to listbox named InventoryContents
			InventoryContents.Items.Add(inv);
		}

		private void InventoryContents_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				int index = InventoryContents.SelectedIndex;
				//MessageBox.Show(items[index].ItemPrice.ToString("c"));
				MessageBox.Show(items[index].ItemName.ToString());//when clicked on in listbox, displays item name
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			
		}
	}
}