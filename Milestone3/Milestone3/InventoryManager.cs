﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone3
{
	class InventoryManager
	{

		InventoryItem[] items = new InventoryItem[5];
		private int valuesInArray = 0; //max number of values in array, intialized to 0
		
		public InventoryManager()
		{
			//create some inventory items
			items[0] = new InventoryItem("Corn", 0.75m);
			items[1] = new InventoryItem("Apple", 1.00m);
			items[2] = new InventoryItem("Soup", 2.25m);
			items[3] = new InventoryItem("Chocolate", 0.85m);
			items[4] = new InventoryItem("Eggs", 0.99m);
			valuesInArray = 5;
		}


		public void add(string name, decimal price)
		{
			//add items to array
			items[valuesInArray] = new InventoryItem(name, price);
			valuesInArray++;
		}


		public void remove()
		{
			//remove selected item (from listbox)  from stock
		}

		public void update()
		{
			//restock items low on quantity
			//when item selected (from listbox) and restock pressed
		}

		public void search()
		{
			//search using 2 criteria (price, quantity, name etc.)
			//search current inventory for specified item
		}

		//display a single item in the inventory.
		public string display(int i)
		{
			return items[i].ToString();//display 1 item based on index passed
		}
	}
}
