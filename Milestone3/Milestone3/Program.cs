﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone3
{
	class Program
	{
		static void Main(string[] args)
		{
			InventoryManager inventory = new InventoryManager();//default object created from inv manager class

			Console.Write(inventory);

			string[] friends = new string[] { "Steven", "Kevin", "Jason L." };
			
			foreach(string friend in friends)
			{
				Console.WriteLine("Hello {0} ", friend);
			}
			

			
			/*
			foreach(InventoryItem inv in items)
			{
				Console.WriteLine("The inventory in the array is {0} ", inv.ItemName);
			}
			*/
		}
	}
}
