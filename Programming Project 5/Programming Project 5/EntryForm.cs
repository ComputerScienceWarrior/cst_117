﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 * ALL WORK IN THIS PROJECT BELONGS TO JAMES-RYAN STAMPLEY
 * AND NO ONE ELSE CAN TAKE CREDIT FOR MY WORK
 * 
 * https://bitbucket.org/ComputerScienceWarrior/cst_117/src/master/
 */

namespace Programming_Project_5
{
	public partial class luckyNumberGenerator : Form
	{		

		//fields for widespread use throughout the class
		int _luckyNumber;  // _luckyNumber is the accumulator

		//(A control populated based on the selected item in another control)
		int _favNumber; //created to change the favNumber entered if certain age choice is made, for more unpredictability
		

		public luckyNumberGenerator()
		{
			InitializeComponent();
		}

		private void generateNumberButton_Click(object sender, EventArgs e)
		{
			//create LuckyNumberForm object
			LuckyNumberForm number = new LuckyNumberForm();

			char favLetter;
			string favColor;
			int age;

			//error handling
			try
			{

			/*
			 *FAVORITE COLOR ENTRY
			 */
				favColor = favColorTextBox.Text;
				if (favColor.Equals(""))
				{
					MessageBox.Show("No value input for favorite color");
				}
				else if(favColor.Equals("red", StringComparison.OrdinalIgnoreCase) || favColor.Equals("green", StringComparison.OrdinalIgnoreCase))
				{
					_luckyNumber = 7;
				}
				else if(favColor.Equals("blue", StringComparison.OrdinalIgnoreCase) || favColor.Equals("yellow", StringComparison.OrdinalIgnoreCase))
				{
					_luckyNumber = 6;
				}
				else if(favColor.Equals("black", StringComparison.OrdinalIgnoreCase) || favColor.Equals("purple", StringComparison.OrdinalIgnoreCase))
				{
					_luckyNumber = 13;
				}
				else
				{
					_luckyNumber = 2;
				}

			/*
			 *FAVORITIE LETTER ENTRY 
			 */
				if (char.TryParse(favLetterTextBox.Text, out favLetter))
				{
					if(favLetter == 'a' || favLetter == 'b' || favLetter == 'c'|| favLetter == 'd' || favLetter == 'e' || favLetter == 'f')
					{
						_luckyNumber += 2;
					}
					else if(favLetter == 'g' || favLetter == 'h' || favLetter == 'i' || favLetter == 'j' || favLetter == 'k' || favLetter == 'l')
					{
						_luckyNumber += 5;
					}
					else if(favLetter == 'm' || favLetter == 'n' || favLetter == 'o' || favLetter == 'p' || favLetter == 'q' || favLetter == 'r')
					{
						_luckyNumber += 1;
					}
					else if(favLetter == 's' || favLetter == 't' || favLetter == 'u' || favLetter == 'v' || favLetter == 'w' || favLetter == 'x')
					{
						_luckyNumber += 3;
					}
					else // covers 'y' and 'z'
					{
						_luckyNumber += 0;
					}
				}
				else
				{
					MessageBox.Show("Incorrect value, please try again.");
					favLetterTextBox.Clear();//clear the field
					favLetterTextBox.Focus(); //set the focus back to enter new letter
				}

			//ENTER FAVORITE NUMBER
				if(int.TryParse(favNumberTextBox.Text, out _favNumber))
				{
					if(_favNumber > 0 && _favNumber <= 7)
					{
						_luckyNumber -= 3;
					}
					else if(_favNumber >= 7 && _favNumber < 20)
					{
						_luckyNumber += 2;
					}
					else
					{
						_luckyNumber /= 2;
					}
				}
				else
				{
					MessageBox.Show("No NUMBER was entered.");
					favNumberTextBox.Clear();//clear the text box
					favNumberTextBox.Focus();//reset focus to enter another number
				}

			//AGE ENTRY
				if(int.TryParse(ageTextBox.Text, out age))
				{
					if(age > 0 && age <= 10) //if between 1 and 10
					{
						_luckyNumber += 2;
					}
					else if(age > 10 && age <= 23)//if between 11 and 23
					{
						_favNumber = 30; //CHANGES THE SELECTION OF FAVORITE NUMBER if ages 11 - 23
						_luckyNumber -= 1;
					}
					else if(age > 24 && age < 40)//if between 25 and 39
					{
						_luckyNumber += 5;
					}
					else //all other ages
					{
						_luckyNumber /= 2;
					}
				}
				else
				{
					MessageBox.Show("No AGE entered.");
				}

				number.luckyNumberLabel.Text = "Your lucky number is...." + _luckyNumber.ToString() + "!!!!";
				number.ShowDialog(); // display the form with lucky number
				
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);//display error message
			}
		}
	}
}
