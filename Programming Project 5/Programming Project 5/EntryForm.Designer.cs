﻿namespace Programming_Project_5
{
	partial class luckyNumberGenerator
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.favColorLabel = new System.Windows.Forms.Label();
			this.favNumberLabel = new System.Windows.Forms.Label();
			this.favLetterLabel = new System.Windows.Forms.Label();
			this.ageLabel = new System.Windows.Forms.Label();
			this.favColorTextBox = new System.Windows.Forms.TextBox();
			this.favNumberTextBox = new System.Windows.Forms.TextBox();
			this.favLetterTextBox = new System.Windows.Forms.TextBox();
			this.ageTextBox = new System.Windows.Forms.TextBox();
			this.generateNumberButton = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// favColorLabel
			// 
			this.favColorLabel.AutoSize = true;
			this.favColorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.favColorLabel.Location = new System.Drawing.Point(56, 66);
			this.favColorLabel.Name = "favColorLabel";
			this.favColorLabel.Size = new System.Drawing.Size(115, 17);
			this.favColorLabel.TabIndex = 6;
			this.favColorLabel.Text = "Favorite Color:";
			// 
			// favNumberLabel
			// 
			this.favNumberLabel.AutoSize = true;
			this.favNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.favNumberLabel.Location = new System.Drawing.Point(56, 141);
			this.favNumberLabel.Name = "favNumberLabel";
			this.favNumberLabel.Size = new System.Drawing.Size(133, 17);
			this.favNumberLabel.TabIndex = 7;
			this.favNumberLabel.Text = "Favorite Number:";
			// 
			// favLetterLabel
			// 
			this.favLetterLabel.AutoSize = true;
			this.favLetterLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.favLetterLabel.Location = new System.Drawing.Point(56, 217);
			this.favLetterLabel.Name = "favLetterLabel";
			this.favLetterLabel.Size = new System.Drawing.Size(115, 17);
			this.favLetterLabel.TabIndex = 8;
			this.favLetterLabel.Text = "Favorite Letter";
			// 
			// ageLabel
			// 
			this.ageLabel.AutoSize = true;
			this.ageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ageLabel.Location = new System.Drawing.Point(56, 290);
			this.ageLabel.Name = "ageLabel";
			this.ageLabel.Size = new System.Drawing.Size(41, 17);
			this.ageLabel.TabIndex = 9;
			this.ageLabel.Text = "Age:";
			// 
			// favColorTextBox
			// 
			this.favColorTextBox.Location = new System.Drawing.Point(291, 66);
			this.favColorTextBox.Name = "favColorTextBox";
			this.favColorTextBox.Size = new System.Drawing.Size(147, 22);
			this.favColorTextBox.TabIndex = 0;
			// 
			// favNumberTextBox
			// 
			this.favNumberTextBox.Location = new System.Drawing.Point(291, 141);
			this.favNumberTextBox.Name = "favNumberTextBox";
			this.favNumberTextBox.Size = new System.Drawing.Size(147, 22);
			this.favNumberTextBox.TabIndex = 1;
			// 
			// favLetterTextBox
			// 
			this.favLetterTextBox.Location = new System.Drawing.Point(291, 217);
			this.favLetterTextBox.Name = "favLetterTextBox";
			this.favLetterTextBox.Size = new System.Drawing.Size(147, 22);
			this.favLetterTextBox.TabIndex = 2;
			// 
			// ageTextBox
			// 
			this.ageTextBox.Location = new System.Drawing.Point(291, 290);
			this.ageTextBox.Name = "ageTextBox";
			this.ageTextBox.Size = new System.Drawing.Size(147, 22);
			this.ageTextBox.TabIndex = 3;
			// 
			// generateNumberButton
			// 
			this.generateNumberButton.AutoSize = true;
			this.generateNumberButton.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.generateNumberButton.ForeColor = System.Drawing.Color.Green;
			this.generateNumberButton.Location = new System.Drawing.Point(135, 346);
			this.generateNumberButton.Name = "generateNumberButton";
			this.generateNumberButton.Size = new System.Drawing.Size(217, 28);
			this.generateNumberButton.TabIndex = 4;
			this.generateNumberButton.Text = "Generate Lucky Number";
			this.generateNumberButton.UseVisualStyleBackColor = true;
			this.generateNumberButton.Click += new System.EventHandler(this.generateNumberButton_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.ForeColor = System.Drawing.Color.Teal;
			this.label5.Location = new System.Drawing.Point(73, 9);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(353, 22);
			this.label5.TabIndex = 5;
			this.label5.Text = "Generate Lucky number with fields below";
			// 
			// luckyNumberGenerator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(496, 386);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.generateNumberButton);
			this.Controls.Add(this.ageTextBox);
			this.Controls.Add(this.favLetterTextBox);
			this.Controls.Add(this.favNumberTextBox);
			this.Controls.Add(this.favColorTextBox);
			this.Controls.Add(this.ageLabel);
			this.Controls.Add(this.favLetterLabel);
			this.Controls.Add(this.favNumberLabel);
			this.Controls.Add(this.favColorLabel);
			this.Name = "luckyNumberGenerator";
			this.Text = "Number Generator Form";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label favColorLabel;
		private System.Windows.Forms.Label favNumberLabel;
		private System.Windows.Forms.Label favLetterLabel;
		private System.Windows.Forms.Label ageLabel;
		private System.Windows.Forms.TextBox favColorTextBox;
		private System.Windows.Forms.TextBox favNumberTextBox;
		private System.Windows.Forms.TextBox favLetterTextBox;
		private System.Windows.Forms.TextBox ageTextBox;
		private System.Windows.Forms.Button generateNumberButton;
		private System.Windows.Forms.Label label5;
	}
}

