﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 * ALL WORK IN THIS PROJECT BELONGS TO JAMES-RYAN STAMPLEY
 * AND NO ONE ELSE CAN TAKE CREDIT FOR MY WORK
 * 
 * https://bitbucket.org/ComputerScienceWarrior/cst_117/src/master/
 */

namespace Programming_Project_5
{
	public partial class LuckyNumberForm : Form
	{
		public LuckyNumberForm()
		{
			InitializeComponent();
		}

		private void closeButton_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
