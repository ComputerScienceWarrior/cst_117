﻿namespace Programming_Project_5
{
	partial class LuckyNumberForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.luckyNumberLabel = new System.Windows.Forms.Label();
			this.closeButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// luckyNumberLabel
			// 
			this.luckyNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.luckyNumberLabel.Location = new System.Drawing.Point(76, 36);
			this.luckyNumberLabel.Name = "luckyNumberLabel";
			this.luckyNumberLabel.Size = new System.Drawing.Size(259, 236);
			this.luckyNumberLabel.TabIndex = 0;
			this.luckyNumberLabel.Text = "Lucky Number";
			this.luckyNumberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// closeButton
			// 
			this.closeButton.AutoSize = true;
			this.closeButton.Location = new System.Drawing.Point(169, 290);
			this.closeButton.Name = "closeButton";
			this.closeButton.Size = new System.Drawing.Size(75, 27);
			this.closeButton.TabIndex = 1;
			this.closeButton.Text = "Close";
			this.closeButton.UseVisualStyleBackColor = true;
			this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
			// 
			// LuckyNumberForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(426, 362);
			this.Controls.Add(this.closeButton);
			this.Controls.Add(this.luckyNumberLabel);
			this.Name = "LuckyNumberForm";
			this.Text = "LuckyNumberForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		public System.Windows.Forms.Label luckyNumberLabel;
		private System.Windows.Forms.Button closeButton;
	}
}