﻿namespace Programming_Project_3
{
	partial class formattingStringsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.saveToFileButton = new System.Windows.Forms.Button();
			this.openFileButton = new System.Windows.Forms.Button();
			this.clearButton = new System.Windows.Forms.Button();
			this.exitButton = new System.Windows.Forms.Button();
			this.openFile = new System.Windows.Forms.OpenFileDialog();
			this.saveFile = new System.Windows.Forms.SaveFileDialog();
			this.displayListBox = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// saveToFileButton
			// 
			this.saveToFileButton.AutoSize = true;
			this.saveToFileButton.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.saveToFileButton.ForeColor = System.Drawing.Color.Green;
			this.saveToFileButton.Location = new System.Drawing.Point(143, 306);
			this.saveToFileButton.Name = "saveToFileButton";
			this.saveToFileButton.Size = new System.Drawing.Size(112, 27);
			this.saveToFileButton.TabIndex = 8;
			this.saveToFileButton.Text = "Save File";
			this.saveToFileButton.UseVisualStyleBackColor = true;
			this.saveToFileButton.Click += new System.EventHandler(this.saveToFileButton_Click);
			// 
			// openFileButton
			// 
			this.openFileButton.AutoSize = true;
			this.openFileButton.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.openFileButton.Location = new System.Drawing.Point(343, 306);
			this.openFileButton.Name = "openFileButton";
			this.openFileButton.Size = new System.Drawing.Size(112, 27);
			this.openFileButton.TabIndex = 9;
			this.openFileButton.Text = "Open File";
			this.openFileButton.UseVisualStyleBackColor = true;
			this.openFileButton.Click += new System.EventHandler(this.openFileButton_Click);
			// 
			// clearButton
			// 
			this.clearButton.AutoSize = true;
			this.clearButton.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.clearButton.Location = new System.Drawing.Point(243, 357);
			this.clearButton.Name = "clearButton";
			this.clearButton.Size = new System.Drawing.Size(112, 27);
			this.clearButton.TabIndex = 10;
			this.clearButton.Text = "Clear";
			this.clearButton.UseVisualStyleBackColor = true;
			this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
			// 
			// exitButton
			// 
			this.exitButton.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.exitButton.ForeColor = System.Drawing.Color.Red;
			this.exitButton.Location = new System.Drawing.Point(243, 415);
			this.exitButton.Name = "exitButton";
			this.exitButton.Size = new System.Drawing.Size(112, 27);
			this.exitButton.TabIndex = 11;
			this.exitButton.Text = "Exit";
			this.exitButton.UseVisualStyleBackColor = true;
			this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
			// 
			// openFile
			// 
			this.openFile.FileName = "openDialogFilename";
			// 
			// saveFile
			// 
			this.saveFile.Filter = "Text File | *.txt";
			// 
			// displayListBox
			// 
			this.displayListBox.FormattingEnabled = true;
			this.displayListBox.ItemHeight = 16;
			this.displayListBox.Location = new System.Drawing.Point(143, 70);
			this.displayListBox.Name = "displayListBox";
			this.displayListBox.Size = new System.Drawing.Size(312, 196);
			this.displayListBox.TabIndex = 12;
			this.displayListBox.SelectedIndexChanged += new System.EventHandler(this.displayListBox_SelectedIndexChanged);
			// 
			// formattingStringsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(630, 450);
			this.Controls.Add(this.displayListBox);
			this.Controls.Add(this.exitButton);
			this.Controls.Add(this.clearButton);
			this.Controls.Add(this.openFileButton);
			this.Controls.Add(this.saveToFileButton);
			this.Name = "formattingStringsForm";
			this.Text = "Formatting Strings with Files";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Button saveToFileButton;
		private System.Windows.Forms.Button openFileButton;
		private System.Windows.Forms.Button clearButton;
		private System.Windows.Forms.Button exitButton;
		private System.Windows.Forms.OpenFileDialog openFile;
		private System.Windows.Forms.SaveFileDialog saveFile;
		private System.Windows.Forms.ListBox displayListBox;
	}
}

