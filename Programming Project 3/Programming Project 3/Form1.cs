﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Programming_Project_3
{
	public partial class formattingStringsForm : Form
	{
		public formattingStringsForm()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void exitButton_Click(object sender, EventArgs e)
		{
			//close the program
			this.Close();
		}

		private void clearButton_Click(object sender, EventArgs e)
		{
			/*
			 *Clear the listbox
			 */
			displayListBox.Items.Clear();
			openFileButton.Focus(); //Set focus back to button allowing user to select file
		}

		private void saveToFileButton_Click(object sender, EventArgs e)
		{
			try
			{
				//streamwriter file created to work with program
				StreamWriter outputFile;

				//write to test doc file
				outputFile = File.CreateText("new file.txt");

				saveFile.InitialDirectory = "C:/Desktop";
				if (saveFile.ShowDialog() == DialogResult.OK)
				{

					foreach (var item in displayListBox.Items)
					{
						outputFile.WriteLine(item);
					}
					outputFile = File.CreateText(saveFile.FileName);
					MessageBox.Show("File saved Successfully!");

				}
				else
				{
					MessageBox.Show("Operation Canceled.");
				}
				outputFile.Close();
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message); //display error that occurred
			}
			
		

		}

		private void openFileButton_Click(object sender, EventArgs e)
		{
			try
			{
				//create streamreader object
				StreamReader inputFile;
				string fileContents; // variable to hold file contents

				//open user file system to prompt file selection
				if (openFile.ShowDialog() == DialogResult.OK)
				{
					//allow user to select a file 
					inputFile = File.OpenText(openFile.FileName);

					while (!inputFile.EndOfStream)
					{
						//display file contents line by line
						fileContents = inputFile.ReadLine();

						//convert to lowercase 
						displayListBox.Items.Add(fileContents.ToString().ToLower());
					}
				}
				else
				{
					MessageBox.Show("Operation Canceled");
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);//catch and display exceptions
			}
		}

		private void displayListBox_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		private void lowercaseButton_Click(object sender, EventArgs e)
		{

		}
	}
}
