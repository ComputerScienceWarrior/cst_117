﻿namespace Exercise_5
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.label1 = new System.Windows.Forms.Label();
			this.calculateButton = new System.Windows.Forms.Button();
			this.clearButton = new System.Windows.Forms.Button();
			this.numberOfTermsTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.exitButton = new System.Windows.Forms.Button();
			this.approximatePi = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(67, 75);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(113, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "Enter # of terms:";
			// 
			// calculateButton
			// 
			this.calculateButton.AutoSize = true;
			this.calculateButton.Location = new System.Drawing.Point(70, 148);
			this.calculateButton.Name = "calculateButton";
			this.calculateButton.Size = new System.Drawing.Size(110, 27);
			this.calculateButton.TabIndex = 1;
			this.calculateButton.Text = "Calculate";
			this.calculateButton.UseVisualStyleBackColor = true;
			this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
			// 
			// clearButton
			// 
			this.clearButton.AutoSize = true;
			this.clearButton.Location = new System.Drawing.Point(70, 241);
			this.clearButton.Name = "clearButton";
			this.clearButton.Size = new System.Drawing.Size(110, 27);
			this.clearButton.TabIndex = 2;
			this.clearButton.Text = "Clear";
			this.clearButton.UseVisualStyleBackColor = true;
			this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
			// 
			// numberOfTermsTextBox
			// 
			this.numberOfTermsTextBox.Location = new System.Drawing.Point(249, 75);
			this.numberOfTermsTextBox.Name = "numberOfTermsTextBox";
			this.numberOfTermsTextBox.Size = new System.Drawing.Size(110, 22);
			this.numberOfTermsTextBox.TabIndex = 0;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(67, 229);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(0, 17);
			this.label2.TabIndex = 3;
			// 
			// exitButton
			// 
			this.exitButton.AutoSize = true;
			this.exitButton.Location = new System.Drawing.Point(249, 241);
			this.exitButton.Name = "exitButton";
			this.exitButton.Size = new System.Drawing.Size(110, 27);
			this.exitButton.TabIndex = 4;
			this.exitButton.Text = "Exit";
			this.exitButton.UseVisualStyleBackColor = true;
			this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
			// 
			// approximatePi
			// 
			this.approximatePi.AutoSize = true;
			this.approximatePi.Location = new System.Drawing.Point(67, 186);
			this.approximatePi.Name = "approximatePi";
			this.approximatePi.Size = new System.Drawing.Size(0, 17);
			this.approximatePi.TabIndex = 5;
			this.approximatePi.Visible = false;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(537, 314);
			this.Controls.Add(this.approximatePi);
			this.Controls.Add(this.exitButton);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.numberOfTermsTextBox);
			this.Controls.Add(this.clearButton);
			this.Controls.Add(this.calculateButton);
			this.Controls.Add(this.label1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Form1";
			this.Text = "Approximate Pi";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button calculateButton;
		private System.Windows.Forms.Button clearButton;
		private System.Windows.Forms.TextBox numberOfTermsTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button exitButton;
		private System.Windows.Forms.Label approximatePi;
	}
}

