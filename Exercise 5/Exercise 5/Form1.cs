﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_5
{
	public partial class Form1 : Form
	{


		public Form1()
		{
			InitializeComponent();
		}

		private void clearButton_Click(object sender, EventArgs e)
		{
			numberOfTermsTextBox.Text = "";
			approximatePi.Text = "";
			numberOfTermsTextBox.Focus(); //reset focus to this textbox
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void exitButton_Click(object sender, EventArgs e)
		{
			this.Close(); //close application
		}

		private void displayPiTextBox_TextChanged(object sender, EventArgs e)
		{

		}

		private void calculateButton_Click(object sender, EventArgs e)
		{
			int numberOfTerms;
			double count; //count made double for purposes of calculating remainder
			decimal pi = 0, bottomValue = 1.0m;

			//validate entry of integer by the user
			if (int.TryParse(numberOfTermsTextBox.Text, out numberOfTerms))
			{
				//MessageBox.Show("Valid Entry!"); //for stepwise test purposes

				//loop through terms to approximate Pi
				for(count = 0; count < numberOfTerms; count++)
				{

					if(count % 2 != 0) //if the count varibale is an odd number
					{
						pi -= (4 / bottomValue); 
					}
					else
					{
						pi += (4 / bottomValue);
					}
					bottomValue += 2; //increment bottom value by 2
				}
				//display pi
				//displayPiTextBox.Text = pi.ToString();
				approximatePi.Text = ("The value of approximate Pi for \n" + numberOfTerms.ToString() + " terms is " + pi.ToString());
				approximatePi.Visible = true;
			}
			else
			{
				MessageBox.Show("Sorry, that is not a valid entry.");
			}

		}
	}
}
