﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 * All code in this project is made by James-Ryan Stampley
 * and no one else can take credit for my work.
 * 
 * https://bitbucket.org/ComputerScienceWarrior/cst_117
 */


namespace Milestone_2
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void printInventoryButton_Click(object sender, EventArgs e)
		{
			//clear inventory contents upon each click to prevent duplicates
			InventoryContents.Items.Clear();

			//create 5 items to add to inventory
			InventoryItem soda = new InventoryItem("Coke");
			InventoryItem chips = new InventoryItem("Plain Lays");
			InventoryItem newspaper = new InventoryItem("Daily Newspaper");
			InventoryItem lighter = new InventoryItem("Red Lighter");
			InventoryItem candybar = new InventoryItem("Snickers");

			//add the items to an array
			InventoryItem[] items = new InventoryItem[5];
			items[0] = soda;
			items[1] = chips;
			items[2] = newspaper;
			items[3] = lighter;
			items[4] = candybar;

			//loop through created items, and add items to the array
			for (int i = 0; i < items.Length; i++)
			{
				InventoryContents.Items.Add(items[i].printItems()); //method call to print items with printItems method
			}
		}

		private void exitButton_Click(object sender, EventArgs e)
		{
			//close program
			this.Close();
		}

		private void clearListbox_Click(object sender, EventArgs e)
		{
			//clear contents of inventory items
			InventoryContents.Items.Clear();
		}
	}
}
