﻿namespace Milestone_2
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.InventoryContents = new System.Windows.Forms.ListBox();
			this.printInventoryButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.exitButton = new System.Windows.Forms.Button();
			this.clearListbox = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// InventoryContents
			// 
			this.InventoryContents.FormattingEnabled = true;
			this.InventoryContents.ItemHeight = 16;
			this.InventoryContents.Location = new System.Drawing.Point(293, 55);
			this.InventoryContents.Name = "InventoryContents";
			this.InventoryContents.Size = new System.Drawing.Size(246, 212);
			this.InventoryContents.TabIndex = 0;
			// 
			// printInventoryButton
			// 
			this.printInventoryButton.AutoSize = true;
			this.printInventoryButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.printInventoryButton.ForeColor = System.Drawing.Color.Green;
			this.printInventoryButton.Location = new System.Drawing.Point(27, 55);
			this.printInventoryButton.Name = "printInventoryButton";
			this.printInventoryButton.Size = new System.Drawing.Size(171, 59);
			this.printInventoryButton.TabIndex = 1;
			this.printInventoryButton.Text = "Print Inventory";
			this.printInventoryButton.UseVisualStyleBackColor = true;
			this.printInventoryButton.Click += new System.EventHandler(this.printInventoryButton_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(80, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(420, 18);
			this.label1.TabIndex = 2;
			this.label1.Text = "Click the button below to print the inventory items";
			// 
			// exitButton
			// 
			this.exitButton.AutoSize = true;
			this.exitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.exitButton.ForeColor = System.Drawing.Color.Red;
			this.exitButton.Location = new System.Drawing.Point(27, 208);
			this.exitButton.Name = "exitButton";
			this.exitButton.Size = new System.Drawing.Size(171, 59);
			this.exitButton.TabIndex = 3;
			this.exitButton.Text = "Exit";
			this.exitButton.UseVisualStyleBackColor = true;
			this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
			// 
			// clearListbox
			// 
			this.clearListbox.AutoSize = true;
			this.clearListbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.clearListbox.ForeColor = System.Drawing.Color.Black;
			this.clearListbox.Location = new System.Drawing.Point(27, 131);
			this.clearListbox.Name = "clearListbox";
			this.clearListbox.Size = new System.Drawing.Size(171, 59);
			this.clearListbox.TabIndex = 4;
			this.clearListbox.Text = "Clear";
			this.clearListbox.UseVisualStyleBackColor = true;
			this.clearListbox.Click += new System.EventHandler(this.clearListbox_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(592, 279);
			this.Controls.Add(this.clearListbox);
			this.Controls.Add(this.exitButton);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.printInventoryButton);
			this.Controls.Add(this.InventoryContents);
			this.Name = "Form1";
			this.Text = "View Inventory Items";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListBox InventoryContents;
		private System.Windows.Forms.Button printInventoryButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button exitButton;
		private System.Windows.Forms.Button clearListbox;
	}
}

