﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Redo_Exercise_6
{
	class Dice
	{
		//Fields
		private int _sideFacing;
		private int _numberOfSides; //will be made read-only 

		//Constructor
		public Dice()
		{
			_numberOfSides = 6; 
		}

		//RollDie method
		public void RollDie()
		{
			//code to roll die
		}

		//Properties
		public int SideFacing { get; set; }

		public int NumberOfSides { get; }
	}
}
