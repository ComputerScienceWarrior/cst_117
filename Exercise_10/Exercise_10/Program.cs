﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

/*
 * ALL work here is my own and No one else can take credit for it
 * https://bitbucket.org/ComputerScienceWarrior/cst_117/src/master/
 * 
 * Given a file, write a program (console app) that counts the 
 * # of words that end w/ letter 't' or 'e' (non-case sensitive)
 * output should be the resulting count
 */

namespace Exercise_10
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				StreamReader inputFile = new StreamReader("C:/Users/codeartist1991/Desktop/GCU CS courses/CST-117/Week 6/Exercise 10/Exercise_10_File.txt");

				string[] textFileArray = inputFile.ReadLine().Split(); //input file is read into the array
				int count = 0;

				//cycle through the array of array declared above
				foreach (string words in textFileArray)
				{
					//remove punctuation from beginning(words) to end(words.Length - 1)
					if (char.IsPunctuation(words, words.Length - 1))
					{
						//create a new string so we can remove the punctuation at the end of the string.
						string newWords = words.Remove(words.Length - 1);

						//look for words ending with t or e, regardless of casing
						if (newWords.EndsWith("t") || newWords.EndsWith("e") ||
							newWords.EndsWith("T") || newWords.EndsWith("E"))
						{
							count++;
						}
					}
					else 
					{
						if (words.EndsWith("t") || words.EndsWith("e") ||
							words.EndsWith("T") || words.EndsWith("E"))
						{
							count++;
						}
					}
				}

				Console.WriteLine(count + " words in the file end in 't' or 'e'");
				inputFile.Close(); //close the file
				Console.ReadLine();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);//show error message
			}
		}
	}
}
