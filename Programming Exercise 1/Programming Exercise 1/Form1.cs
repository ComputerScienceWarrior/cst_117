﻿/*
 * Bit bucket repository link for James-Ryan Stampley:
 * https://bitbucket.org/ComputerScienceWarrior/cst_117/src/master/
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Exercise_1
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void closeProgram_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void conversionButton_Click(object sender, EventArgs e)
		{
			//NOTE: 1 inch is 2.54 cm

			/*convert users string input to a double
			 * and perform conversion calculation
			 * */
double inches = double.Parse(inchesInput.Text);
			double conversion = inches * 2.54;

			/*convert double value of 'conversion' back
			 * to a string to output it to a textbox 
			 */
			centimeterOutput.Text = conversion.ToString("F3");//formatted to exactly 3 decimals
		}

		private void divideNumber_Click(object sender, EventArgs e)
		{

			try
			{
				//numbers used to calculate result
				double number1, number2, divisionResult;

				/*
				 * convert entered text numbers to double values
				 */
				number1 = double.Parse(numberOne.Text);
				number2 = double.Parse(numberTwo.Text);

				divisionResult = number1 / number2;

				//convert result back to a string for textbox output
				result.Text = divisionResult.ToString("F3"); //formatted to exactly 3 decimal places
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message); //display exception message
			}

		}
	}
}
