﻿namespace Programming_Exercise_1
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.enterInchesLabel = new System.Windows.Forms.Label();
			this.conversionButton = new System.Windows.Forms.Button();
			this.inchesInput = new System.Windows.Forms.TextBox();
			this.centimeterOutputLabel = new System.Windows.Forms.Label();
			this.centimeterOutput = new System.Windows.Forms.TextBox();
			this.closeProgram = new System.Windows.Forms.Button();
			this.divideNumber = new System.Windows.Forms.Button();
			this.numberOne = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.numberTwo = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.result = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// enterInchesLabel
			// 
			this.enterInchesLabel.AutoSize = true;
			this.enterInchesLabel.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.enterInchesLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
			this.enterInchesLabel.Location = new System.Drawing.Point(79, 39);
			this.enterInchesLabel.Name = "enterInchesLabel";
			this.enterInchesLabel.Size = new System.Drawing.Size(137, 20);
			this.enterInchesLabel.TabIndex = 0;
			this.enterInchesLabel.Text = "Enter Inches:";
			// 
			// conversionButton
			// 
			this.conversionButton.AutoSize = true;
			this.conversionButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.conversionButton.Location = new System.Drawing.Point(282, 114);
			this.conversionButton.Name = "conversionButton";
			this.conversionButton.Size = new System.Drawing.Size(93, 30);
			this.conversionButton.TabIndex = 1;
			this.conversionButton.Text = "Convert";
			this.conversionButton.UseVisualStyleBackColor = true;
			this.conversionButton.Click += new System.EventHandler(this.conversionButton_Click);
			// 
			// inchesInput
			// 
			this.inchesInput.Location = new System.Drawing.Point(354, 37);
			this.inchesInput.Name = "inchesInput";
			this.inchesInput.Size = new System.Drawing.Size(186, 22);
			this.inchesInput.TabIndex = 2;
			this.inchesInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// centimeterOutputLabel
			// 
			this.centimeterOutputLabel.AutoSize = true;
			this.centimeterOutputLabel.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.centimeterOutputLabel.ForeColor = System.Drawing.SystemColors.HotTrack;
			this.centimeterOutputLabel.Location = new System.Drawing.Point(79, 79);
			this.centimeterOutputLabel.Name = "centimeterOutputLabel";
			this.centimeterOutputLabel.Size = new System.Drawing.Size(131, 20);
			this.centimeterOutputLabel.TabIndex = 3;
			this.centimeterOutputLabel.Text = "Centimeters:";
			// 
			// centimeterOutput
			// 
			this.centimeterOutput.Location = new System.Drawing.Point(354, 77);
			this.centimeterOutput.Name = "centimeterOutput";
			this.centimeterOutput.ReadOnly = true;
			this.centimeterOutput.Size = new System.Drawing.Size(186, 22);
			this.centimeterOutput.TabIndex = 4;
			this.centimeterOutput.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// closeProgram
			// 
			this.closeProgram.AutoSize = true;
			this.closeProgram.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.closeProgram.Location = new System.Drawing.Point(392, 289);
			this.closeProgram.Name = "closeProgram";
			this.closeProgram.Size = new System.Drawing.Size(75, 30);
			this.closeProgram.TabIndex = 5;
			this.closeProgram.Text = "Close";
			this.closeProgram.UseVisualStyleBackColor = true;
			this.closeProgram.Click += new System.EventHandler(this.closeProgram_Click);
			// 
			// divideNumber
			// 
			this.divideNumber.AutoSize = true;
			this.divideNumber.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.divideNumber.Location = new System.Drawing.Point(188, 289);
			this.divideNumber.Name = "divideNumber";
			this.divideNumber.Size = new System.Drawing.Size(75, 30);
			this.divideNumber.TabIndex = 6;
			this.divideNumber.Text = "Divide";
			this.divideNumber.UseVisualStyleBackColor = true;
			this.divideNumber.Click += new System.EventHandler(this.divideNumber_Click);
			// 
			// numberOne
			// 
			this.numberOne.Location = new System.Drawing.Point(353, 163);
			this.numberOne.Name = "numberOne";
			this.numberOne.Size = new System.Drawing.Size(186, 22);
			this.numberOne.TabIndex = 7;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.SystemColors.Control;
			this.label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.label1.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
			this.label1.Location = new System.Drawing.Point(79, 165);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(236, 20);
			this.label1.TabIndex = 8;
			this.label1.Text = "Enter number to divide:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
			this.label2.Location = new System.Drawing.Point(79, 203);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(184, 20);
			this.label2.TabIndex = 9;
			this.label2.Text = "Divide number by:";
			// 
			// numberTwo
			// 
			this.numberTwo.Location = new System.Drawing.Point(353, 203);
			this.numberTwo.Name = "numberTwo";
			this.numberTwo.Size = new System.Drawing.Size(186, 22);
			this.numberTwo.TabIndex = 10;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
			this.label3.Location = new System.Drawing.Point(79, 246);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(76, 20);
			this.label3.TabIndex = 11;
			this.label3.Text = "Result:";
			// 
			// result
			// 
			this.result.Location = new System.Drawing.Point(353, 247);
			this.result.Name = "result";
			this.result.ReadOnly = true;
			this.result.Size = new System.Drawing.Size(185, 22);
			this.result.TabIndex = 12;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(626, 336);
			this.Controls.Add(this.result);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.numberTwo);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.numberOne);
			this.Controls.Add(this.divideNumber);
			this.Controls.Add(this.closeProgram);
			this.Controls.Add(this.centimeterOutput);
			this.Controls.Add(this.centimeterOutputLabel);
			this.Controls.Add(this.inchesInput);
			this.Controls.Add(this.conversionButton);
			this.Controls.Add(this.enterInchesLabel);
			this.Name = "Form1";
			this.Text = "Convert Inches to Centimeters and Divide Numbers";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label enterInchesLabel;
		private System.Windows.Forms.Button conversionButton;
		private System.Windows.Forms.TextBox inchesInput;
		private System.Windows.Forms.Label centimeterOutputLabel;
		private System.Windows.Forms.TextBox centimeterOutput;
		private System.Windows.Forms.Button closeProgram;
		private System.Windows.Forms.Button divideNumber;
		private System.Windows.Forms.TextBox numberOne;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox numberTwo;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox result;
	}
}

