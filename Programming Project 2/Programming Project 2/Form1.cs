﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//bitbucket repository: https://bitbucket.org/ComputerScienceWarrior/cst_117/src/master/

/*
 * Intorductory form meant only for illustrating certain functions of listboxes, radio buttons and Checkboxes
 * Not yet ready for full use
 */

namespace Programming_Project_2
{
	public partial class registrationForm : Form
	{
		public registrationForm()
		{
			InitializeComponent();
		}

		private void exitButton_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void clearButton_Click(object sender, EventArgs e)
		{
			//clear the fields upon clicking the button
			firstNameTextBox.Text = "";
			lastNameTextBox.Text = "";
			ageTextBox.Text = "";
			cityTextBox.Text = "";
			emailTextBox.Text = "";

			//clear radio button selections and checkboxes
			male.Checked = false;
			female.Checked = false;
			noAnswer.Checked = false;
			newsLetterCheckBox.Checked = false;
			agreementCheckBox.Checked = false;

			stateListBox.SelectedIndex = -1; //clear selection of state
			campusCityLabel.Visible = false; // set label back to invisible
			firstNameTextBox.Focus(); //resets focus back to first textbox
		}

		private void stateListBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			
		}

		private void showCampusCity_Click(object sender, EventArgs e)
		{
			string campusCity;

			if (stateListBox.SelectedIndex != -1)
			{
				campusCity = stateListBox.SelectedItem.ToString();
				switch (campusCity)
				{
					case "Arizona":
						campusCityLabel.Visible = true;
						campusCityLabel.Text = "Campus in Phoenix";
						break;
					case "California":
						campusCityLabel.Visible = true;
						campusCityLabel.Text = "Campus in San Diego";
						break;
					case "Colorado":
						campusCityLabel.Visible = true;
						campusCityLabel.Text = "Campus in Denver";
						break; 
					case "Nevada":
						campusCityLabel.Visible = true;
						campusCityLabel.Text = "Campus in Las Vegas";
						break;
					case "Utah":
						campusCityLabel.Visible = true;
						campusCityLabel.Text = "Campus in Layton";
						break;
				}
			}
			else
			{
				MessageBox.Show("Nothing selected, please select a state first.");
			}
		}

		private void submitButton_Click(object sender, EventArgs e)
		{
			int age;

			//properly prevent non-number entry for age
			if(int.TryParse(ageTextBox.Text, out age))
			{
				//do not allow form to be submitted without checking agreement checkbox
				if(!(agreementCheckBox.Checked))
				{
					MessageBox.Show("Please agree to terms to complete submission.");
				}
				else
				{
					//proceed with submission
					MessageBox.Show("Submission Successful!");

					//clear the fields upon clicking the submit button
					firstNameTextBox.Text = "";
					lastNameTextBox.Text = "";
					ageTextBox.Text = "";
					cityTextBox.Text = "";
					emailTextBox.Text = "";

					//clear radio button selections and checkboxes upon submitting
					male.Checked = false;
					female.Checked = false;
					noAnswer.Checked = false;
					newsLetterCheckBox.Checked = false;
					agreementCheckBox.Checked = false;

					stateListBox.SelectedIndex = -1; //clear selection of state
					campusCityLabel.Visible = false; // set label back to invisible
					firstNameTextBox.Focus(); //resets focus back to first textbox
				}
			}
			else
			{
				MessageBox.Show("Age entered is not an actual number, try again.");
				ageTextBox.Text = ""; //clear age
			}
		}

		private void male_CheckedChanged(object sender, EventArgs e)
		{
			//turn Gender label pink upon selection
			if (male.Checked)
				genderLabel.ForeColor = Color.Blue;
		}

		private void female_CheckedChanged(object sender, EventArgs e)
		{
			//turn Gender label blue upon selection
			if (female.Checked)
				genderLabel.ForeColor = Color.HotPink;
		}

		private void noAnswer_CheckedChanged(object sender, EventArgs e)
		{
			//ensure standard black font color if no gender selected
			if (noAnswer.Checked)
				genderLabel.ForeColor = Color.Black;
		}
	}
}
