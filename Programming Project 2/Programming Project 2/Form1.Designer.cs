﻿namespace Programming_Project_2
{
	partial class registrationForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.submitButton = new System.Windows.Forms.Button();
			this.clearButton = new System.Windows.Forms.Button();
			this.exitButton = new System.Windows.Forms.Button();
			this.promptLabel = new System.Windows.Forms.Label();
			this.firstNameLabel = new System.Windows.Forms.Label();
			this.secondNameLabel = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.cityLabel = new System.Windows.Forms.Label();
			this.stateLabel = new System.Windows.Forms.Label();
			this.genderLabel = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.offersAndAgreements = new System.Windows.Forms.GroupBox();
			this.agreementCheckBox = new System.Windows.Forms.CheckBox();
			this.newsLetterCheckBox = new System.Windows.Forms.CheckBox();
			this.male = new System.Windows.Forms.RadioButton();
			this.female = new System.Windows.Forms.RadioButton();
			this.noAnswer = new System.Windows.Forms.RadioButton();
			this.firstNameTextBox = new System.Windows.Forms.TextBox();
			this.lastNameTextBox = new System.Windows.Forms.TextBox();
			this.ageTextBox = new System.Windows.Forms.TextBox();
			this.cityTextBox = new System.Windows.Forms.TextBox();
			this.stateListBox = new System.Windows.Forms.ListBox();
			this.emailTextBox = new System.Windows.Forms.TextBox();
			this.campusCityLabel = new System.Windows.Forms.Label();
			this.showCampusCity = new System.Windows.Forms.Button();
			this.offersAndAgreements.SuspendLayout();
			this.SuspendLayout();
			// 
			// submitButton
			// 
			this.submitButton.AutoSize = true;
			this.submitButton.Location = new System.Drawing.Point(36, 525);
			this.submitButton.Name = "submitButton";
			this.submitButton.Size = new System.Drawing.Size(75, 27);
			this.submitButton.TabIndex = 0;
			this.submitButton.Text = "&Submit";
			this.submitButton.UseVisualStyleBackColor = true;
			this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
			// 
			// clearButton
			// 
			this.clearButton.AutoSize = true;
			this.clearButton.Location = new System.Drawing.Point(300, 525);
			this.clearButton.Name = "clearButton";
			this.clearButton.Size = new System.Drawing.Size(75, 27);
			this.clearButton.TabIndex = 1;
			this.clearButton.Text = "&Clear";
			this.clearButton.UseVisualStyleBackColor = true;
			this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
			// 
			// exitButton
			// 
			this.exitButton.AutoSize = true;
			this.exitButton.Location = new System.Drawing.Point(579, 525);
			this.exitButton.Name = "exitButton";
			this.exitButton.Size = new System.Drawing.Size(75, 27);
			this.exitButton.TabIndex = 2;
			this.exitButton.Text = "&Exit";
			this.exitButton.UseVisualStyleBackColor = true;
			this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
			// 
			// promptLabel
			// 
			this.promptLabel.AutoSize = true;
			this.promptLabel.Font = new System.Drawing.Font("Verdana", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.promptLabel.ForeColor = System.Drawing.Color.DodgerBlue;
			this.promptLabel.Location = new System.Drawing.Point(165, 9);
			this.promptLabel.Name = "promptLabel";
			this.promptLabel.Size = new System.Drawing.Size(340, 23);
			this.promptLabel.TabIndex = 3;
			this.promptLabel.Text = "Please fill out all fields below:";
			// 
			// firstNameLabel
			// 
			this.firstNameLabel.AutoSize = true;
			this.firstNameLabel.Location = new System.Drawing.Point(33, 69);
			this.firstNameLabel.Name = "firstNameLabel";
			this.firstNameLabel.Size = new System.Drawing.Size(97, 17);
			this.firstNameLabel.TabIndex = 4;
			this.firstNameLabel.Text = "1) First Name:";
			// 
			// secondNameLabel
			// 
			this.secondNameLabel.AutoSize = true;
			this.secondNameLabel.Location = new System.Drawing.Point(33, 122);
			this.secondNameLabel.Name = "secondNameLabel";
			this.secondNameLabel.Size = new System.Drawing.Size(97, 17);
			this.secondNameLabel.TabIndex = 5;
			this.secondNameLabel.Text = "2) Last Name:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(33, 174);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(54, 17);
			this.label4.TabIndex = 6;
			this.label4.Text = "3) Age:";
			// 
			// cityLabel
			// 
			this.cityLabel.AutoSize = true;
			this.cityLabel.Location = new System.Drawing.Point(33, 226);
			this.cityLabel.Name = "cityLabel";
			this.cityLabel.Size = new System.Drawing.Size(52, 17);
			this.cityLabel.TabIndex = 7;
			this.cityLabel.Text = "4) City:";
			// 
			// stateLabel
			// 
			this.stateLabel.AutoSize = true;
			this.stateLabel.Location = new System.Drawing.Point(33, 263);
			this.stateLabel.Name = "stateLabel";
			this.stateLabel.Size = new System.Drawing.Size(249, 17);
			this.stateLabel.TabIndex = 8;
			this.stateLabel.Text = "5) Select state campus you will attend:";
			// 
			// genderLabel
			// 
			this.genderLabel.AutoSize = true;
			this.genderLabel.Location = new System.Drawing.Point(33, 357);
			this.genderLabel.Name = "genderLabel";
			this.genderLabel.Size = new System.Drawing.Size(77, 17);
			this.genderLabel.TabIndex = 9;
			this.genderLabel.Text = "6) Gender:";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(33, 411);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(119, 17);
			this.label8.TabIndex = 10;
			this.label8.Text = "7) Email Address:";
			// 
			// offersAndAgreements
			// 
			this.offersAndAgreements.Controls.Add(this.agreementCheckBox);
			this.offersAndAgreements.Controls.Add(this.newsLetterCheckBox);
			this.offersAndAgreements.Location = new System.Drawing.Point(36, 431);
			this.offersAndAgreements.Name = "offersAndAgreements";
			this.offersAndAgreements.Size = new System.Drawing.Size(618, 72);
			this.offersAndAgreements.TabIndex = 11;
			this.offersAndAgreements.TabStop = false;
			// 
			// agreementCheckBox
			// 
			this.agreementCheckBox.AutoSize = true;
			this.agreementCheckBox.Location = new System.Drawing.Point(6, 18);
			this.agreementCheckBox.Name = "agreementCheckBox";
			this.agreementCheckBox.Size = new System.Drawing.Size(463, 21);
			this.agreementCheckBox.TabIndex = 12;
			this.agreementCheckBox.Text = "I understand and agree to all terms and conditions of this submission";
			this.agreementCheckBox.UseVisualStyleBackColor = true;
			// 
			// newsLetterCheckBox
			// 
			this.newsLetterCheckBox.AutoSize = true;
			this.newsLetterCheckBox.Location = new System.Drawing.Point(6, 45);
			this.newsLetterCheckBox.Name = "newsLetterCheckBox";
			this.newsLetterCheckBox.Size = new System.Drawing.Size(302, 21);
			this.newsLetterCheckBox.TabIndex = 13;
			this.newsLetterCheckBox.Text = "Sign me up for email offers and newsletters";
			this.newsLetterCheckBox.UseVisualStyleBackColor = true;
			// 
			// male
			// 
			this.male.AutoSize = true;
			this.male.Location = new System.Drawing.Point(231, 357);
			this.male.Name = "male";
			this.male.Size = new System.Drawing.Size(59, 21);
			this.male.TabIndex = 12;
			this.male.TabStop = true;
			this.male.Text = "Male";
			this.male.UseVisualStyleBackColor = true;
			this.male.CheckedChanged += new System.EventHandler(this.male_CheckedChanged);
			// 
			// female
			// 
			this.female.AutoSize = true;
			this.female.Location = new System.Drawing.Point(361, 357);
			this.female.Name = "female";
			this.female.Size = new System.Drawing.Size(75, 21);
			this.female.TabIndex = 13;
			this.female.TabStop = true;
			this.female.Text = "Female";
			this.female.UseVisualStyleBackColor = true;
			this.female.CheckedChanged += new System.EventHandler(this.female_CheckedChanged);
			// 
			// noAnswer
			// 
			this.noAnswer.AutoSize = true;
			this.noAnswer.Location = new System.Drawing.Point(497, 357);
			this.noAnswer.Name = "noAnswer";
			this.noAnswer.Size = new System.Drawing.Size(157, 21);
			this.noAnswer.TabIndex = 14;
			this.noAnswer.TabStop = true;
			this.noAnswer.Text = "Prefer not to answer";
			this.noAnswer.UseVisualStyleBackColor = true;
			this.noAnswer.CheckedChanged += new System.EventHandler(this.noAnswer_CheckedChanged);
			// 
			// firstNameTextBox
			// 
			this.firstNameTextBox.Location = new System.Drawing.Point(231, 69);
			this.firstNameTextBox.Name = "firstNameTextBox";
			this.firstNameTextBox.Size = new System.Drawing.Size(195, 22);
			this.firstNameTextBox.TabIndex = 16;
			// 
			// lastNameTextBox
			// 
			this.lastNameTextBox.Location = new System.Drawing.Point(231, 122);
			this.lastNameTextBox.Name = "lastNameTextBox";
			this.lastNameTextBox.Size = new System.Drawing.Size(195, 22);
			this.lastNameTextBox.TabIndex = 17;
			// 
			// ageTextBox
			// 
			this.ageTextBox.Location = new System.Drawing.Point(231, 174);
			this.ageTextBox.Name = "ageTextBox";
			this.ageTextBox.Size = new System.Drawing.Size(195, 22);
			this.ageTextBox.TabIndex = 18;
			// 
			// cityTextBox
			// 
			this.cityTextBox.Location = new System.Drawing.Point(231, 226);
			this.cityTextBox.Name = "cityTextBox";
			this.cityTextBox.Size = new System.Drawing.Size(195, 22);
			this.cityTextBox.TabIndex = 19;
			// 
			// stateListBox
			// 
			this.stateListBox.FormattingEnabled = true;
			this.stateListBox.ItemHeight = 16;
			this.stateListBox.Items.AddRange(new object[] {
            "Arizona",
            "California",
            "Colorado",
            "Nevada",
            "Utah"});
			this.stateListBox.Location = new System.Drawing.Point(300, 263);
			this.stateListBox.Name = "stateListBox";
			this.stateListBox.Size = new System.Drawing.Size(120, 84);
			this.stateListBox.TabIndex = 20;
			this.stateListBox.SelectedIndexChanged += new System.EventHandler(this.stateListBox_SelectedIndexChanged);
			// 
			// emailTextBox
			// 
			this.emailTextBox.Location = new System.Drawing.Point(231, 406);
			this.emailTextBox.Name = "emailTextBox";
			this.emailTextBox.Size = new System.Drawing.Size(195, 22);
			this.emailTextBox.TabIndex = 21;
			// 
			// campusCityLabel
			// 
			this.campusCityLabel.AutoSize = true;
			this.campusCityLabel.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.campusCityLabel.Location = new System.Drawing.Point(476, 293);
			this.campusCityLabel.Name = "campusCityLabel";
			this.campusCityLabel.Size = new System.Drawing.Size(104, 18);
			this.campusCityLabel.TabIndex = 22;
			this.campusCityLabel.Text = "Display City";
			this.campusCityLabel.Visible = false;
			// 
			// showCampusCity
			// 
			this.showCampusCity.AutoSize = true;
			this.showCampusCity.Location = new System.Drawing.Point(479, 263);
			this.showCampusCity.Name = "showCampusCity";
			this.showCampusCity.Size = new System.Drawing.Size(154, 27);
			this.showCampusCity.TabIndex = 23;
			this.showCampusCity.Text = "Campus City Location";
			this.showCampusCity.UseVisualStyleBackColor = true;
			this.showCampusCity.Click += new System.EventHandler(this.showCampusCity_Click);
			// 
			// registrationForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(695, 576);
			this.Controls.Add(this.showCampusCity);
			this.Controls.Add(this.campusCityLabel);
			this.Controls.Add(this.emailTextBox);
			this.Controls.Add(this.stateListBox);
			this.Controls.Add(this.cityTextBox);
			this.Controls.Add(this.ageTextBox);
			this.Controls.Add(this.lastNameTextBox);
			this.Controls.Add(this.firstNameTextBox);
			this.Controls.Add(this.noAnswer);
			this.Controls.Add(this.female);
			this.Controls.Add(this.male);
			this.Controls.Add(this.offersAndAgreements);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.genderLabel);
			this.Controls.Add(this.stateLabel);
			this.Controls.Add(this.cityLabel);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.secondNameLabel);
			this.Controls.Add(this.firstNameLabel);
			this.Controls.Add(this.promptLabel);
			this.Controls.Add(this.exitButton);
			this.Controls.Add(this.clearButton);
			this.Controls.Add(this.submitButton);
			this.Name = "registrationForm";
			this.Text = "School Campus Selection Form";
			this.offersAndAgreements.ResumeLayout(false);
			this.offersAndAgreements.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button submitButton;
		private System.Windows.Forms.Button clearButton;
		private System.Windows.Forms.Button exitButton;
		private System.Windows.Forms.Label promptLabel;
		private System.Windows.Forms.Label firstNameLabel;
		private System.Windows.Forms.Label secondNameLabel;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label cityLabel;
		private System.Windows.Forms.Label stateLabel;
		private System.Windows.Forms.Label genderLabel;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.GroupBox offersAndAgreements;
		private System.Windows.Forms.CheckBox agreementCheckBox;
		private System.Windows.Forms.CheckBox newsLetterCheckBox;
		private System.Windows.Forms.RadioButton male;
		private System.Windows.Forms.RadioButton female;
		private System.Windows.Forms.RadioButton noAnswer;
		private System.Windows.Forms.TextBox firstNameTextBox;
		private System.Windows.Forms.TextBox lastNameTextBox;
		private System.Windows.Forms.TextBox ageTextBox;
		private System.Windows.Forms.TextBox cityTextBox;
		private System.Windows.Forms.ListBox stateListBox;
		private System.Windows.Forms.TextBox emailTextBox;
		private System.Windows.Forms.Label campusCityLabel;
		private System.Windows.Forms.Button showCampusCity;
	}
}

