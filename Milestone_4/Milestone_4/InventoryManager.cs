﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone_4
{
	class InventoryManager
	{
		List<InventoryItem> products = new List<InventoryItem>();

		
		public InventoryManager()
		{
			//default constructor
		}

		public void add(List<InventoryItem> list, InventoryItem item)
		{
			products.Add(item);
		}

		public void remove(List<InventoryItem> list,InventoryItem item)
		{
			
		}

		public int update(InventoryItem item)
		{
			//restock items quantity
			return item.ItemQuantity;
		}

		public void search(List<InventoryItem> list, InventoryItem name)
		{
			//search using item name
			for(int i = 0; i < list.Count; i++)
			{
				if (list.Contains(name))
				{
					MessageBox.Show("Item " + name.ItemName + " found in Inventory!");
				}
			}
		}

		public string display(InventoryItem item)
		{
			//display the item name and the price of the item
			
			return item.ItemName + ": " + "$" + item.ItemPrice;
		}
		public void display(List<InventoryItem> list, InventoryItem item)
		{
			MessageBox.Show(item.ToString());
		}

		public void display(List<InventoryItem> list, string item)
		{
			MessageBox.Show(item.ToString());
		}
	}
}
