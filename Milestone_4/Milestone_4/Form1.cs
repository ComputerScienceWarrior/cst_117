﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Milestone_4
{
	public partial class Form1 : Form
	{
		//list of inventory items
		List<InventoryItem> products = new List<InventoryItem>();

		//Inventory Manager
		InventoryManager inventory = new InventoryManager();

		//closing message for Management System
		//instantiate object
		Close message = new Close();

		int index;
		int quantity;

		public Form1()
		{
			InitializeComponent();
		}

		private void exitButton_Click(object sender, EventArgs e)
		{
			//display close message
			message.ShowDialog();
			//EXIT THE PROGRAM
			this.Close();
		}

		private void clearButton_Click(object sender, EventArgs e)
		{
			//CLEAR THE LISTBOX
			InventoryContents.Items.Clear();
		}

		private void addButton_Click(object sender, EventArgs e)
		{

			//temporary values for name, quantity and price of products
			string name;
			decimal price;
			

			//error handling for adding the product information
			try
			{
				if(decimal.TryParse(addPrice.Text, out price))
				{
					if(int.TryParse(addQuantity.Text, out quantity))
					{
						name = addName.Text;
						inventory.add(products, new InventoryItem(name, price, quantity));
						products.Add(new InventoryItem(name, price, quantity));

						//show message confirming item added
						MessageBox.Show("Item added successfully!");
					}
					else
					{
						
						MessageBox.Show("Incorrect value for quantity, try again.");
					}
				}
				else
				{
					MessageBox.Show("Incorrect value for Price, try again.");
					addPrice.Text = "";
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);//display exception
			}
			
			for(int i = 0; i < products.Count; i++)
			{
				inventory.add(products, products[i]);//loop through array and add item to 'products' array
				InventoryContents.Items.Add(products[i]);//add to list
			}

			//clear textboxes
			addName.Text = "";
			addPrice.Text = "";
			addQuantity.Text = "";

			//clear Listbox
			InventoryContents.Items.Clear();
		}

		private void InventoryContents_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				index = InventoryContents.SelectedIndex;
				//MessageBox.Show(items[index].ItemPrice.ToString("c"));
				MessageBox.Show("Quantity Available: " + products[index].ItemQuantity.ToString());//when clicked on in listbox, displays item name
				
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);//display error message
			}
		}

		private void displayButton_Click(object sender, EventArgs e)
		{

			if(products.Count == 0)
			{
				MessageBox.Show("No products to display!");
			}
			else if (products.Count >= 1)
			{
				//clear the contents and reprint upon each click to prevent duplicates
				InventoryContents.Items.Clear();

				//display inventory products
				foreach (InventoryItem item in products)
				{
					inventory.display(products, item.ItemName);
					InventoryContents.Items.Add(inventory.display(item));//display inventory using display method
				}
			}
		}

		private void removeButton_Click(object sender, EventArgs e)
		{
			
		}

		private void updateButton_Click(object sender, EventArgs e)
		{
			
		}

		private void searchButton_Click(object sender, EventArgs e)
		{
			/*
			 * Only issue with search is the casing.
			 */
			foreach(InventoryItem item in products)
			{
				if(searchName.Text == item.ItemName)//search name entered in search text box
				{
					inventory.search(products, item);//search all items in inventory
				}
				else if(searchName.Text != item.ItemName)//if search name entered is invalid..
				{
					MessageBox.Show("Item " + searchName.Text + " not found!");
				}
				
			}
			
			//search inventory list 'products' for name entered in text box
			
		}
	}
}
