﻿namespace Exercise_8
{
	partial class caloriesCalculatorForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fatGramsLabel = new System.Windows.Forms.Label();
			this.resultingCaloriesFromFatLabel = new System.Windows.Forms.Label();
			this.calculateCaloriesButton = new System.Windows.Forms.Button();
			this.clearFatGramsButton = new System.Windows.Forms.Button();
			this.exitButton = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.fatGramsTextBox = new System.Windows.Forms.TextBox();
			this.resultingCaloriesTextBox = new System.Windows.Forms.TextBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.calculateCaloriesButton2 = new System.Windows.Forms.Button();
			this.clearCarbGramsButton = new System.Windows.Forms.Button();
			this.carbGramsTextBox = new System.Windows.Forms.TextBox();
			this.resultingCaloriesTextBox2 = new System.Windows.Forms.TextBox();
			this.resultingCaloriesFromCarbsLabel = new System.Windows.Forms.Label();
			this.carbGramsLabel = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// fatGramsLabel
			// 
			this.fatGramsLabel.AutoSize = true;
			this.fatGramsLabel.Location = new System.Drawing.Point(6, 53);
			this.fatGramsLabel.Name = "fatGramsLabel";
			this.fatGramsLabel.Size = new System.Drawing.Size(109, 17);
			this.fatGramsLabel.TabIndex = 0;
			this.fatGramsLabel.Text = "Enter fat grams:";
			// 
			// resultingCaloriesFromFatLabel
			// 
			this.resultingCaloriesFromFatLabel.AutoSize = true;
			this.resultingCaloriesFromFatLabel.Location = new System.Drawing.Point(6, 112);
			this.resultingCaloriesFromFatLabel.Name = "resultingCaloriesFromFatLabel";
			this.resultingCaloriesFromFatLabel.Size = new System.Drawing.Size(126, 17);
			this.resultingCaloriesFromFatLabel.TabIndex = 1;
			this.resultingCaloriesFromFatLabel.Text = "Resulting Calories:";
			// 
			// calculateCaloriesButton
			// 
			this.calculateCaloriesButton.AutoSize = true;
			this.calculateCaloriesButton.Location = new System.Drawing.Point(76, 157);
			this.calculateCaloriesButton.Name = "calculateCaloriesButton";
			this.calculateCaloriesButton.Size = new System.Drawing.Size(76, 27);
			this.calculateCaloriesButton.TabIndex = 2;
			this.calculateCaloriesButton.Text = "Calculate";
			this.calculateCaloriesButton.UseVisualStyleBackColor = true;
			this.calculateCaloriesButton.Click += new System.EventHandler(this.calculateCaloriesButton_Click);
			// 
			// clearFatGramsButton
			// 
			this.clearFatGramsButton.AutoSize = true;
			this.clearFatGramsButton.Location = new System.Drawing.Point(221, 157);
			this.clearFatGramsButton.Name = "clearFatGramsButton";
			this.clearFatGramsButton.Size = new System.Drawing.Size(75, 27);
			this.clearFatGramsButton.TabIndex = 3;
			this.clearFatGramsButton.Text = "Clear";
			this.clearFatGramsButton.UseVisualStyleBackColor = true;
			this.clearFatGramsButton.Click += new System.EventHandler(this.clearFatGramsButton_Click);
			// 
			// exitButton
			// 
			this.exitButton.AutoSize = true;
			this.exitButton.Location = new System.Drawing.Point(311, 462);
			this.exitButton.Name = "exitButton";
			this.exitButton.Size = new System.Drawing.Size(75, 27);
			this.exitButton.TabIndex = 4;
			this.exitButton.Text = "Exit";
			this.exitButton.UseVisualStyleBackColor = true;
			this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.fatGramsTextBox);
			this.groupBox1.Controls.Add(this.resultingCaloriesTextBox);
			this.groupBox1.Controls.Add(this.fatGramsLabel);
			this.groupBox1.Controls.Add(this.clearFatGramsButton);
			this.groupBox1.Controls.Add(this.resultingCaloriesFromFatLabel);
			this.groupBox1.Controls.Add(this.calculateCaloriesButton);
			this.groupBox1.Location = new System.Drawing.Point(155, 21);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(388, 190);
			this.groupBox1.TabIndex = 5;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Calculate calories from Fat";
			// 
			// fatGramsTextBox
			// 
			this.fatGramsTextBox.Location = new System.Drawing.Point(221, 53);
			this.fatGramsTextBox.Name = "fatGramsTextBox";
			this.fatGramsTextBox.Size = new System.Drawing.Size(144, 22);
			this.fatGramsTextBox.TabIndex = 6;
			// 
			// resultingCaloriesTextBox
			// 
			this.resultingCaloriesTextBox.Location = new System.Drawing.Point(221, 112);
			this.resultingCaloriesTextBox.Name = "resultingCaloriesTextBox";
			this.resultingCaloriesTextBox.ReadOnly = true;
			this.resultingCaloriesTextBox.Size = new System.Drawing.Size(144, 22);
			this.resultingCaloriesTextBox.TabIndex = 7;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.calculateCaloriesButton2);
			this.groupBox2.Controls.Add(this.clearCarbGramsButton);
			this.groupBox2.Controls.Add(this.carbGramsTextBox);
			this.groupBox2.Controls.Add(this.resultingCaloriesTextBox2);
			this.groupBox2.Controls.Add(this.resultingCaloriesFromCarbsLabel);
			this.groupBox2.Controls.Add(this.carbGramsLabel);
			this.groupBox2.Location = new System.Drawing.Point(155, 254);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(388, 190);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Calculate calories from Carbs";
			// 
			// calculateCaloriesButton2
			// 
			this.calculateCaloriesButton2.AutoSize = true;
			this.calculateCaloriesButton2.Location = new System.Drawing.Point(76, 161);
			this.calculateCaloriesButton2.Name = "calculateCaloriesButton2";
			this.calculateCaloriesButton2.Size = new System.Drawing.Size(76, 27);
			this.calculateCaloriesButton2.TabIndex = 6;
			this.calculateCaloriesButton2.Text = "Calculate";
			this.calculateCaloriesButton2.UseVisualStyleBackColor = true;
			this.calculateCaloriesButton2.Click += new System.EventHandler(this.calculateCaloriesButton2_Click);
			// 
			// clearCarbGramsButton
			// 
			this.clearCarbGramsButton.AutoSize = true;
			this.clearCarbGramsButton.Location = new System.Drawing.Point(221, 161);
			this.clearCarbGramsButton.Name = "clearCarbGramsButton";
			this.clearCarbGramsButton.Size = new System.Drawing.Size(75, 27);
			this.clearCarbGramsButton.TabIndex = 7;
			this.clearCarbGramsButton.Text = "Clear";
			this.clearCarbGramsButton.UseVisualStyleBackColor = true;
			this.clearCarbGramsButton.Click += new System.EventHandler(this.clearCarbGramsButton_Click);
			// 
			// carbGramsTextBox
			// 
			this.carbGramsTextBox.Location = new System.Drawing.Point(221, 45);
			this.carbGramsTextBox.Name = "carbGramsTextBox";
			this.carbGramsTextBox.Size = new System.Drawing.Size(144, 22);
			this.carbGramsTextBox.TabIndex = 8;
			// 
			// resultingCaloriesTextBox2
			// 
			this.resultingCaloriesTextBox2.Location = new System.Drawing.Point(221, 109);
			this.resultingCaloriesTextBox2.Name = "resultingCaloriesTextBox2";
			this.resultingCaloriesTextBox2.ReadOnly = true;
			this.resultingCaloriesTextBox2.Size = new System.Drawing.Size(144, 22);
			this.resultingCaloriesTextBox2.TabIndex = 9;
			// 
			// resultingCaloriesFromCarbsLabel
			// 
			this.resultingCaloriesFromCarbsLabel.AutoSize = true;
			this.resultingCaloriesFromCarbsLabel.Location = new System.Drawing.Point(6, 109);
			this.resultingCaloriesFromCarbsLabel.Name = "resultingCaloriesFromCarbsLabel";
			this.resultingCaloriesFromCarbsLabel.Size = new System.Drawing.Size(126, 17);
			this.resultingCaloriesFromCarbsLabel.TabIndex = 1;
			this.resultingCaloriesFromCarbsLabel.Text = "Resulting Calories:";
			// 
			// carbGramsLabel
			// 
			this.carbGramsLabel.AutoSize = true;
			this.carbGramsLabel.Location = new System.Drawing.Point(6, 50);
			this.carbGramsLabel.Name = "carbGramsLabel";
			this.carbGramsLabel.Size = new System.Drawing.Size(121, 17);
			this.carbGramsLabel.TabIndex = 0;
			this.carbGramsLabel.Text = "Enter carb grams:";
			// 
			// caloriesCalculatorForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 511);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.exitButton);
			this.Name = "caloriesCalculatorForm";
			this.Text = "Calories from Fat and Carbohydrates";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label fatGramsLabel;
		private System.Windows.Forms.Label resultingCaloriesFromFatLabel;
		private System.Windows.Forms.Button calculateCaloriesButton;
		private System.Windows.Forms.Button clearFatGramsButton;
		private System.Windows.Forms.Button exitButton;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label resultingCaloriesFromCarbsLabel;
		private System.Windows.Forms.Label carbGramsLabel;
		private System.Windows.Forms.TextBox fatGramsTextBox;
		private System.Windows.Forms.TextBox resultingCaloriesTextBox;
		private System.Windows.Forms.Button calculateCaloriesButton2;
		private System.Windows.Forms.Button clearCarbGramsButton;
		private System.Windows.Forms.TextBox carbGramsTextBox;
		private System.Windows.Forms.TextBox resultingCaloriesTextBox2;
	}
}

