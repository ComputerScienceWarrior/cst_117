﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//This work all belongs to James-Ryan Stampley and NO ONE else can take credit for my work
//https://bitbucket.org/ComputerScienceWarrior/cst_117


/*
Calories from Fat and Carbohydrates

A nutritionist who works for a fitness club helps members by evaluating their diets.
As part of her evaluation, she asks members for the number of fat grams and
carbohydrate grams that they consume in a day. Then, she calculates the number of
calories that result from the fat using the following formula:
Calories from fat = Fat grams × 9
Next, she calculates the number of calories that result from the carbohydrates using
the following formula:
Calories from carbs = Carbs grams × 4
Create an application that will make these calculations. In the application, you
should have the following methods:
***FatCalories–This method should accept a number of fat grams as an argument
and return the number of calories from that amount of fat.
***CarbCalories–This method should accept a number of carbohydrate
grams as an argument and return the number of calories from that amount
of carbohydrates.
*/

namespace Exercise_8
{
	public partial class caloriesCalculatorForm : Form
	{
		//value-returning method that calculates the number of calories from fat grams
		//caloriesFromFat = fatGrams * 9
		public double FatCalories(double fatGrams)
		{
			//create and initialize caloriesFromFat variable
			double caloriesFromFat = fatGrams * 9;

			return caloriesFromFat; //return the calories from fat
		}

		//value-returning method that calculates the number of calories from carbohydrates grams
		//caloriesFromCarbs = carbGrams * 4;
		public double CarbsCalories(double carbGrams)
		{
			//create and initialize caloriesFromCarbs vairable
			double caloriesFromCarbs = carbGrams * 4;

			return caloriesFromCarbs; //returns the calories from the carbs
		}

		public caloriesCalculatorForm()
		{
			InitializeComponent();
		}

		private void exitButton_Click(object sender, EventArgs e)
		{
			//close the program
			this.Close();
		}

		private void calculateCaloriesButton_Click(object sender, EventArgs e)
		{
			double fatGrams; //fat grams variable declaration

			//convert fat grams entered in text box to an integer
			if(double.TryParse(fatGramsTextBox.Text, out fatGrams))
			{
				//method call to FatCalories method
				//result is input in appropriate textbox
				resultingCaloriesTextBox.Text = FatCalories(fatGrams).ToString();
			}
			else
			{
				//error message for inproper input
				MessageBox.Show("Sorry, that input is not accepted. Enter a NUMBER.");

				//clear textbox and reset focus
				fatGramsTextBox.Text = "";
				fatGramsTextBox.Focus();
			}
		}

		private void clearFatGramsButton_Click(object sender, EventArgs e)
		{
			//clear the following fields upon clicking the clear button:
			fatGramsTextBox.Text = "";
			resultingCaloriesTextBox.Text = "";

			fatGramsTextBox.Focus(); // reset focus to enter new amount of Fat Grams
		}

		private void calculateCaloriesButton2_Click(object sender, EventArgs e)
		{
			//declare variable for carb grams
			double carbGrams;

			//if entry is valid, continue calculation
			if(double.TryParse(carbGramsTextBox.Text, out carbGrams))
			{
				//take the number input from the textbox, and convert it to a string
				//then assign the textbox the result from the method call
				resultingCaloriesTextBox2.Text = CarbsCalories(carbGrams).ToString();
			}
			else
			{
				//error message for invalid input
				MessageBox.Show("Sorry, that's not even CLOSE to a number. Please enter a NUMBER.");

				//clear invalid input and reset focus to enter again
				carbGramsTextBox.Text = "";
				carbGramsTextBox.Focus();
			}
		}

		private void clearCarbGramsButton_Click(object sender, EventArgs e)
		{
			//clear entries upon button click
			carbGramsTextBox.Text = "";
			resultingCaloriesTextBox2.Text = "";

			//reset focus for new entry
			carbGramsTextBox.Focus();
		}
	}
}
