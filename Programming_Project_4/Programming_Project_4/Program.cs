﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 * All work here is my own and No one can take credit for my work 
 * Tic tac toe console game using 2-Dim array
 * Version 1.0
 */ 

namespace Programming_Project_4
{
	class Program
	{
		//used for 'draw' purposes  
		//there can only be 9 turns without a win, otherwise a draw
		static int turnCounter;//counts turns taken in a game

		//tic tac toe game
		static string[,] field = new string[3, 3] { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };// 3 x 3 gameboard with 2-dim array

		static void Main(string[] args)
		{
			int input = 0;
			int player = 2;
			bool validSelection = true;

			GameBoard();//gameboard for tic-tac-toe
			do
			{
				//player selection
				if (player == 2)
				{
					player = 1;
					XorO(player, input);//get player input based on player
				}
				else if(player == 1)
				{
					player = 2;
					XorO(player, input);//get player input based on player
				}

				//gameboard for tic-tac-toe
				GameBoard();

				//check winning condition
				string[] symbols = { "X", "O" };

				/*
				 *8 conditions of a player winning 
				 */
				foreach (string playerSymbol in symbols)
				{
					if (((field[0, 0] == playerSymbol) && (field[0, 1] == playerSymbol) && (field[0, 2] == playerSymbol))
						|| ((field[1, 0] == playerSymbol) && (field[1, 1] == playerSymbol) && (field[1, 2] == playerSymbol))
						|| ((field[2, 0] == playerSymbol) && (field[2, 1] == playerSymbol) && (field[2, 2] == playerSymbol))
						|| ((field[0, 0] == playerSymbol) && (field[1, 0] == playerSymbol) && (field[2, 0] == playerSymbol))
						|| ((field[0, 1] == playerSymbol) && (field[1, 1] == playerSymbol) && (field[2, 1] == playerSymbol))
						|| ((field[0, 2] == playerSymbol) && (field[1, 2] == playerSymbol) && (field[2, 2] == playerSymbol))
						|| ((field[0, 0] == playerSymbol) && (field[1, 1] == playerSymbol) && (field[2, 2] == playerSymbol))
						|| ((field[2, 0] == playerSymbol) && (field[1, 1] == playerSymbol) && (field[0, 2] == playerSymbol)))
					{
						if(playerSymbol == "X")
						{
							Console.WriteLine("\nThe winner is player 2.");
						}
						else
						{
							Console.WriteLine("\nPlayer 1 wins.");
						}
						Console.WriteLine("Press a key to reset the game.");
						Console.ReadKey();
						//reset the playing field
						reset();

						break;
					}
					else if (turnCounter == 10)
					{
						Console.WriteLine("We have a draw!");
						Console.WriteLine("Press a key to reset the game.");
						Console.ReadKey();
						//reset the playing field
						reset();
					}
				}

				do
				{
					//prompt user input
					Console.WriteLine("Player {0}: choose ", player);

					//error handling for incorrect input
					try
					{
						input = Convert.ToInt32(Console.ReadLine()); //convert user input to integer
					}
					catch
					{
						Console.WriteLine("please Enter a number");
					}

					//field is selectable if there is not an X or O in the gameboard field
					if ((input == 1) && (field[0, 0] == "1"))
						validSelection = true;
					else if ((input == 2) && (field[0, 1] == "2"))
						validSelection = true;
					else if ((input == 3) && (field[0, 2] == "3"))
						validSelection = true;
					else if ((input == 4) && (field[1, 0] == "4"))
						validSelection = true;
					else if ((input == 5) && (field[1, 1] == "5"))
						validSelection = true;
					else if ((input == 6) && (field[1, 2] == "6"))
						validSelection = true;
					else if ((input == 7) && (field[2, 0] == "7"))
						validSelection = true;
					else if ((input == 8) && (field[2, 1] == "8"))
						validSelection = true;
					else if ((input == 9) && (field[2, 2] == "9"))
						validSelection = true;
					else
					{
						Console.WriteLine("\nInput is invalid, please enter another field");
						validSelection = false;
					}


				}


				//verify correct user input
				while (!validSelection);


			}
			while (true);

		}

		//this method resets back to original play field
		public static void reset()
		{
			//tic tac toe game original values
			string[,] fieldInitiallySet = new string[3, 3] { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };// 3 x 3 gameboard with 2-dim array

			field = fieldInitiallySet;
			GameBoard();
			turnCounter = 0;//reset to 0 upon resetting
		}
		public static void GameBoard()
		{
			Console.Clear();
			Console.WriteLine("     |     |     ");
			Console.WriteLine("  {0}  |  {1}  |  {2}   ", field[0,0], field[0, 1], field[0, 2]); //set positions
			Console.WriteLine("_____|_____|_____");
			Console.WriteLine("     |     |     ");
			Console.WriteLine("  {0}  |  {1}  |  {2}   ", field[1, 0], field[1, 1], field[1, 2]);
			Console.WriteLine("_____|_____|_____");
			Console.WriteLine("     |     |     ");
			Console.WriteLine("  {0}  |  {1}  |  {2}   ", field[2, 0], field[2, 1], field[2, 2]);
			Console.WriteLine("     |     |     ");

			turnCounter++;//count the total turns taken by both players
		}

		public static void XorO(int player, int input)
		{
			string playerCharacter = "";

			//determines whether player 1 or 2, and chooses appropriate symbol.
			if (player == 1)
			{
				playerCharacter = "X";
			}
			else if (player == 2)
			{
				playerCharacter = "O";
			}

			switch (input)
			{
				case 1: field[0, 0] = playerCharacter; break;//fill position 0,0 with 'X' for player 1 selection
				case 2: field[0, 1] = playerCharacter; break;//fill position 0,1 with 'X' for player 1 selection
				case 3: field[0, 2] = playerCharacter; break;//fill position 0,2 with 'X' for player 1 selection
				case 4: field[1, 0] = playerCharacter; break;//fill position 1,0 with 'X' for player 1 selection
				case 5: field[1, 1] = playerCharacter; break;//fill position 1,1 with 'X' for player 1 selection	
				case 6: field[1, 2] = playerCharacter; break;//fill position 1,2 with 'X' for player 1 selection
				case 7: field[2, 0] = playerCharacter; break;//fill position 2,0 with 'X' for player 1 selection
				case 8: field[2, 1] = playerCharacter; break;//fill position 2,1 with 'X' for player 1 selection
				case 9: field[2, 2] = playerCharacter; break;//fill position 2,2 with 'X' for player 1 selection
			}
			
		}
	}
}
