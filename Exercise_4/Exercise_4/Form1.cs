﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_4
{
	public partial class Form1 : Form
	{
		//declare field variables
		int seconds, secondsToMinutes, secondsToHours, secondsToDays;

		private void clearButton_Click(object sender, EventArgs e)
		{
			//clears all input upon clicking the 'Clear' button
			secondsElapsedTextBox.Text = "";
			minutesElapsedTextBox.Text = "";
			hoursElapsedTextBox.Text = "";
			daysElapsedTextBox.Text = "";

			secondsElapsedTextBox.Focus();//reset focus to prepare for more user input
		}

		public Form1()
		{
			InitializeComponent();
		}

		private void exitButton_Click(object sender, EventArgs e)
		{
			//close the application
			this.Close();
		}

		private void calculateButton_Click(object sender, EventArgs e)
		{
			
			if(int.TryParse(secondsElapsedTextBox.Text, out seconds))//prevent invalid entry for seconds 
			{
				if((seconds >= 60) && (seconds < 3600))
				{
					//continue with program execution if proper value is enetered
					secondsToMinutes = seconds / 60; //convert seconds enetered in text box to minutes by dividing seconds by 60;

					//print the minutes in the minutes elapsed textbox on form
					minutesElapsedTextBox.Text = secondsToMinutes.ToString();

					//ensure all other textboxes are cleared to eliminate confusion
					hoursElapsedTextBox.Text = "";
					daysElapsedTextBox.Text = "";

				}
				else if((seconds >= 3600) && (seconds < 86400))
				{
					//determine number of hours in seconds entered
					secondsToHours = (seconds / 60) / 60;

					//print the hours in the hours elapsed textbox on the form
					hoursElapsedTextBox.Text = secondsToHours.ToString();

					//ensure other textboxes cleared to eliminate confusion
					minutesElapsedTextBox.Text = "";
					daysElapsedTextBox.Text = "";

				}
				else if((seconds >= 86400))
				{
					//determine number of days within the seconds entered
					secondsToDays = (seconds / 86400);

					//print the days in the days elapsed textbox on the form
					daysElapsedTextBox.Text = secondsToDays.ToString();

					//clear other textboxes
					minutesElapsedTextBox.Text = "";
					hoursElapsedTextBox.Text = "";

				}
			}
			else
			{
				MessageBox.Show("Invalid input, please enter an integer.");
				secondsElapsedTextBox.Text = "";
				secondsElapsedTextBox.Focus(); //set focus to re-enter seconds 
			}

		}
	}
}
