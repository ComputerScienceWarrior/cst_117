﻿namespace Exercise_4
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.secondsElapsedLabel = new System.Windows.Forms.Label();
			this.minutesElapsedLabel = new System.Windows.Forms.Label();
			this.minutesElapsedTextBox = new System.Windows.Forms.TextBox();
			this.secondsElapsedTextBox = new System.Windows.Forms.TextBox();
			this.daysElapsedTextBox = new System.Windows.Forms.TextBox();
			this.hoursElapsedTextBox = new System.Windows.Forms.TextBox();
			this.hoursElapsedLabel = new System.Windows.Forms.Label();
			this.daysElapsedLabel = new System.Windows.Forms.Label();
			this.calculateButton = new System.Windows.Forms.Button();
			this.clearButton = new System.Windows.Forms.Button();
			this.exitButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.Tomato;
			this.label1.Location = new System.Drawing.Point(29, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(370, 20);
			this.label1.TabIndex = 7;
			this.label1.Text = "Enter the number of seconds elapsed:";
			// 
			// secondsElapsedLabel
			// 
			this.secondsElapsedLabel.AutoSize = true;
			this.secondsElapsedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.secondsElapsedLabel.Location = new System.Drawing.Point(12, 63);
			this.secondsElapsedLabel.Name = "secondsElapsedLabel";
			this.secondsElapsedLabel.Size = new System.Drawing.Size(138, 17);
			this.secondsElapsedLabel.TabIndex = 8;
			this.secondsElapsedLabel.Text = "Seconds Elapsed:";
			// 
			// minutesElapsedLabel
			// 
			this.minutesElapsedLabel.AutoSize = true;
			this.minutesElapsedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.minutesElapsedLabel.Location = new System.Drawing.Point(12, 100);
			this.minutesElapsedLabel.Name = "minutesElapsedLabel";
			this.minutesElapsedLabel.Size = new System.Drawing.Size(132, 17);
			this.minutesElapsedLabel.TabIndex = 9;
			this.minutesElapsedLabel.Text = "Minutes Elapsed:";
			// 
			// minutesElapsedTextBox
			// 
			this.minutesElapsedTextBox.Location = new System.Drawing.Point(255, 100);
			this.minutesElapsedTextBox.Name = "minutesElapsedTextBox";
			this.minutesElapsedTextBox.ReadOnly = true;
			this.minutesElapsedTextBox.Size = new System.Drawing.Size(144, 22);
			this.minutesElapsedTextBox.TabIndex = 1;
			// 
			// secondsElapsedTextBox
			// 
			this.secondsElapsedTextBox.Location = new System.Drawing.Point(255, 63);
			this.secondsElapsedTextBox.Name = "secondsElapsedTextBox";
			this.secondsElapsedTextBox.Size = new System.Drawing.Size(144, 22);
			this.secondsElapsedTextBox.TabIndex = 0;
			// 
			// daysElapsedTextBox
			// 
			this.daysElapsedTextBox.Location = new System.Drawing.Point(255, 169);
			this.daysElapsedTextBox.Name = "daysElapsedTextBox";
			this.daysElapsedTextBox.ReadOnly = true;
			this.daysElapsedTextBox.Size = new System.Drawing.Size(144, 22);
			this.daysElapsedTextBox.TabIndex = 3;
			// 
			// hoursElapsedTextBox
			// 
			this.hoursElapsedTextBox.Location = new System.Drawing.Point(255, 134);
			this.hoursElapsedTextBox.Name = "hoursElapsedTextBox";
			this.hoursElapsedTextBox.ReadOnly = true;
			this.hoursElapsedTextBox.Size = new System.Drawing.Size(144, 22);
			this.hoursElapsedTextBox.TabIndex = 2;
			// 
			// hoursElapsedLabel
			// 
			this.hoursElapsedLabel.AutoSize = true;
			this.hoursElapsedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.hoursElapsedLabel.Location = new System.Drawing.Point(12, 134);
			this.hoursElapsedLabel.Name = "hoursElapsedLabel";
			this.hoursElapsedLabel.Size = new System.Drawing.Size(119, 17);
			this.hoursElapsedLabel.TabIndex = 10;
			this.hoursElapsedLabel.Text = "Hours Elapsed:";
			// 
			// daysElapsedLabel
			// 
			this.daysElapsedLabel.AutoSize = true;
			this.daysElapsedLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.daysElapsedLabel.Location = new System.Drawing.Point(12, 169);
			this.daysElapsedLabel.Name = "daysElapsedLabel";
			this.daysElapsedLabel.Size = new System.Drawing.Size(112, 17);
			this.daysElapsedLabel.TabIndex = 11;
			this.daysElapsedLabel.Text = "Days Elapsed:";
			// 
			// calculateButton
			// 
			this.calculateButton.AutoSize = true;
			this.calculateButton.Location = new System.Drawing.Point(12, 228);
			this.calculateButton.Name = "calculateButton";
			this.calculateButton.Size = new System.Drawing.Size(76, 27);
			this.calculateButton.TabIndex = 4;
			this.calculateButton.Text = "&Calculate";
			this.calculateButton.UseVisualStyleBackColor = true;
			this.calculateButton.Click += new System.EventHandler(this.calculateButton_Click);
			// 
			// clearButton
			// 
			this.clearButton.AutoSize = true;
			this.clearButton.Location = new System.Drawing.Point(169, 228);
			this.clearButton.Name = "clearButton";
			this.clearButton.Size = new System.Drawing.Size(75, 27);
			this.clearButton.TabIndex = 5;
			this.clearButton.Text = "Clear";
			this.clearButton.UseVisualStyleBackColor = true;
			this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
			// 
			// exitButton
			// 
			this.exitButton.AutoSize = true;
			this.exitButton.Location = new System.Drawing.Point(324, 228);
			this.exitButton.Name = "exitButton";
			this.exitButton.Size = new System.Drawing.Size(75, 27);
			this.exitButton.TabIndex = 6;
			this.exitButton.Text = "&Exit";
			this.exitButton.UseVisualStyleBackColor = true;
			this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(453, 263);
			this.Controls.Add(this.exitButton);
			this.Controls.Add(this.clearButton);
			this.Controls.Add(this.calculateButton);
			this.Controls.Add(this.daysElapsedLabel);
			this.Controls.Add(this.hoursElapsedLabel);
			this.Controls.Add(this.hoursElapsedTextBox);
			this.Controls.Add(this.daysElapsedTextBox);
			this.Controls.Add(this.secondsElapsedTextBox);
			this.Controls.Add(this.minutesElapsedTextBox);
			this.Controls.Add(this.minutesElapsedLabel);
			this.Controls.Add(this.secondsElapsedLabel);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "Seconds Converter";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label secondsElapsedLabel;
		private System.Windows.Forms.Label minutesElapsedLabel;
		private System.Windows.Forms.TextBox minutesElapsedTextBox;
		private System.Windows.Forms.TextBox secondsElapsedTextBox;
		private System.Windows.Forms.TextBox daysElapsedTextBox;
		private System.Windows.Forms.TextBox hoursElapsedTextBox;
		private System.Windows.Forms.Label hoursElapsedLabel;
		private System.Windows.Forms.Label daysElapsedLabel;
		private System.Windows.Forms.Button calculateButton;
		private System.Windows.Forms.Button clearButton;
		private System.Windows.Forms.Button exitButton;
	}
}

