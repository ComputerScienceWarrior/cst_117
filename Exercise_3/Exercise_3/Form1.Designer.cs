﻿namespace Exercise_3
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.inchesLabel = new System.Windows.Forms.Label();
			this.convert = new System.Windows.Forms.Button();
			this.centimeterOutput = new System.Windows.Forms.TextBox();
			this.exit = new System.Windows.Forms.Button();
			this.centimetersLabel = new System.Windows.Forms.Label();
			this.inchesEntered = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// inchesLabel
			// 
			this.inchesLabel.AutoSize = true;
			this.inchesLabel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.inchesLabel.Location = new System.Drawing.Point(88, 48);
			this.inchesLabel.Name = "inchesLabel";
			this.inchesLabel.Size = new System.Drawing.Size(124, 23);
			this.inchesLabel.TabIndex = 0;
			this.inchesLabel.Text = "Enter Inches:";
			// 
			// convert
			// 
			this.convert.Location = new System.Drawing.Point(128, 232);
			this.convert.Name = "convert";
			this.convert.Size = new System.Drawing.Size(75, 23);
			this.convert.TabIndex = 1;
			this.convert.Text = "convert";
			this.convert.UseVisualStyleBackColor = true;
			this.convert.Click += new System.EventHandler(this.convert_Click);
			// 
			// centimeterOutput
			// 
			this.centimeterOutput.Location = new System.Drawing.Point(231, 121);
			this.centimeterOutput.Name = "centimeterOutput";
			this.centimeterOutput.Size = new System.Drawing.Size(161, 22);
			this.centimeterOutput.TabIndex = 2;
			// 
			// exit
			// 
			this.exit.Location = new System.Drawing.Point(256, 232);
			this.exit.Name = "exit";
			this.exit.Size = new System.Drawing.Size(75, 23);
			this.exit.TabIndex = 3;
			this.exit.Text = "exit";
			this.exit.UseVisualStyleBackColor = true;
			this.exit.Click += new System.EventHandler(this.exit_Click);
			// 
			// centimetersLabel
			// 
			this.centimetersLabel.AutoSize = true;
			this.centimetersLabel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.centimetersLabel.Location = new System.Drawing.Point(91, 121);
			this.centimetersLabel.Name = "centimetersLabel";
			this.centimetersLabel.Size = new System.Drawing.Size(121, 23);
			this.centimetersLabel.TabIndex = 4;
			this.centimetersLabel.Text = "Centimeters:";
			// 
			// inchesEntered
			// 
			this.inchesEntered.Location = new System.Drawing.Point(231, 48);
			this.inchesEntered.Name = "inchesEntered";
			this.inchesEntered.Size = new System.Drawing.Size(161, 22);
			this.inchesEntered.TabIndex = 5;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(440, 301);
			this.Controls.Add(this.inchesEntered);
			this.Controls.Add(this.centimetersLabel);
			this.Controls.Add(this.exit);
			this.Controls.Add(this.centimeterOutput);
			this.Controls.Add(this.convert);
			this.Controls.Add(this.inchesLabel);
			this.Name = "Form1";
			this.Text = "Convert Inches to Centimeters";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label inchesLabel;
		private System.Windows.Forms.Button convert;
		private System.Windows.Forms.TextBox centimeterOutput;
		private System.Windows.Forms.Button exit;
		private System.Windows.Forms.Label centimetersLabel;
		private System.Windows.Forms.TextBox inchesEntered;
	}
}

