﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_3
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void convert_Click(object sender, EventArgs e)
		{
			//NOTE: 1 inch is 2.54 cm

			/*convert users string input to a double
			 * and perform conversion calculation
			 * */
			double inches = double.Parse(inchesEntered.Text);
			double conversion = inches * 2.54;

			/*convert double value of 'conversion' back
			 * to a string to output it to a textbox 
			 */
			centimeterOutput.Text = conversion.ToString();
		}

		private void exit_Click(object sender, EventArgs e)
		{
			//exit program
			this.Close();
		}
	}
}
