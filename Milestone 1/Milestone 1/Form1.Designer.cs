﻿namespace Milestone_1
{
	partial class homePage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.addButton = new System.Windows.Forms.Button();
			this.removeButton = new System.Windows.Forms.Button();
			this.searchButton = new System.Windows.Forms.Button();
			this.editButton = new System.Windows.Forms.Button();
			this.viewButton = new System.Windows.Forms.Button();
			this.reorderButton = new System.Windows.Forms.Button();
			this.exitButton = new System.Windows.Forms.Button();
			this.promptLabel = new System.Windows.Forms.Label();
			this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.addProductName = new System.Windows.Forms.TextBox();
			this.addProductPrice = new System.Windows.Forms.TextBox();
			this.addProductQty = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.searchByName = new System.Windows.Forms.TextBox();
			this.addInStock = new System.Windows.Forms.TextBox();
			this.addProductVendor = new System.Windows.Forms.TextBox();
			this.addProductCalories = new System.Windows.Forms.TextBox();
			this.addProductType = new System.Windows.Forms.TextBox();
			this.removeByName = new System.Windows.Forms.TextBox();
			this.editByName = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
			this.SuspendLayout();
			// 
			// addButton
			// 
			this.addButton.AutoSize = true;
			this.addButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.addButton.ForeColor = System.Drawing.Color.Green;
			this.addButton.Location = new System.Drawing.Point(42, 116);
			this.addButton.Name = "addButton";
			this.addButton.Size = new System.Drawing.Size(119, 30);
			this.addButton.TabIndex = 0;
			this.addButton.Text = "ADD";
			this.addButton.UseVisualStyleBackColor = true;
			// 
			// removeButton
			// 
			this.removeButton.AutoSize = true;
			this.removeButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.removeButton.ForeColor = System.Drawing.Color.Red;
			this.removeButton.Location = new System.Drawing.Point(42, 172);
			this.removeButton.Name = "removeButton";
			this.removeButton.Size = new System.Drawing.Size(119, 30);
			this.removeButton.TabIndex = 1;
			this.removeButton.Text = "REMOVE";
			this.removeButton.UseVisualStyleBackColor = true;
			// 
			// searchButton
			// 
			this.searchButton.AutoSize = true;
			this.searchButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.searchButton.Location = new System.Drawing.Point(42, 229);
			this.searchButton.Name = "searchButton";
			this.searchButton.Size = new System.Drawing.Size(119, 30);
			this.searchButton.TabIndex = 2;
			this.searchButton.Text = "SEARCH";
			this.searchButton.UseVisualStyleBackColor = true;
			// 
			// editButton
			// 
			this.editButton.AutoSize = true;
			this.editButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.editButton.Location = new System.Drawing.Point(42, 289);
			this.editButton.Name = "editButton";
			this.editButton.Size = new System.Drawing.Size(119, 30);
			this.editButton.TabIndex = 3;
			this.editButton.Text = "EDIT";
			this.editButton.UseVisualStyleBackColor = true;
			// 
			// viewButton
			// 
			this.viewButton.AutoSize = true;
			this.viewButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.viewButton.Location = new System.Drawing.Point(342, 378);
			this.viewButton.Name = "viewButton";
			this.viewButton.Size = new System.Drawing.Size(119, 30);
			this.viewButton.TabIndex = 4;
			this.viewButton.Text = "VIEW";
			this.viewButton.UseVisualStyleBackColor = true;
			// 
			// reorderButton
			// 
			this.reorderButton.AutoSize = true;
			this.reorderButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.reorderButton.ForeColor = System.Drawing.Color.Green;
			this.reorderButton.Location = new System.Drawing.Point(639, 378);
			this.reorderButton.Name = "reorderButton";
			this.reorderButton.Size = new System.Drawing.Size(119, 30);
			this.reorderButton.TabIndex = 5;
			this.reorderButton.Text = "RE-ORDER";
			this.reorderButton.UseVisualStyleBackColor = true;
			// 
			// exitButton
			// 
			this.exitButton.AutoSize = true;
			this.exitButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.exitButton.Location = new System.Drawing.Point(493, 468);
			this.exitButton.Name = "exitButton";
			this.exitButton.Size = new System.Drawing.Size(119, 30);
			this.exitButton.TabIndex = 6;
			this.exitButton.Text = "Exit";
			this.exitButton.UseVisualStyleBackColor = true;
			this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
			// 
			// promptLabel
			// 
			this.promptLabel.AutoSize = true;
			this.promptLabel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.promptLabel.ForeColor = System.Drawing.SystemColors.MenuHighlight;
			this.promptLabel.Location = new System.Drawing.Point(314, 9);
			this.promptLabel.Name = "promptLabel";
			this.promptLabel.Size = new System.Drawing.Size(411, 23);
			this.promptLabel.TabIndex = 7;
			this.promptLabel.Text = "Please choose from the following options below:";
			// 
			// fileSystemWatcher1
			// 
			this.fileSystemWatcher1.EnableRaisingEvents = true;
			this.fileSystemWatcher1.SynchronizingObject = this;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(203, 80);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(117, 17);
			this.label1.TabIndex = 8;
			this.label1.Text = "Product Name";
			this.label1.Click += new System.EventHandler(this.label1_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(367, 80);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(46, 17);
			this.label2.TabIndex = 9;
			this.label2.Text = "Price";
			this.label2.Click += new System.EventHandler(this.label2_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(479, 80);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(74, 17);
			this.label3.TabIndex = 10;
			this.label3.Text = "Quantity";
			this.label3.Click += new System.EventHandler(this.label3_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(619, 80);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(46, 17);
			this.label4.TabIndex = 11;
			this.label4.Text = "Type";
			this.label4.Click += new System.EventHandler(this.label4_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(733, 80);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(70, 17);
			this.label5.TabIndex = 12;
			this.label5.Text = "Calories";
			this.label5.Click += new System.EventHandler(this.label5_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(853, 80);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(65, 17);
			this.label6.TabIndex = 13;
			this.label6.Text = "Vendor";
			this.label6.Click += new System.EventHandler(this.label6_Click);
			// 
			// addProductName
			// 
			this.addProductName.Location = new System.Drawing.Point(206, 122);
			this.addProductName.Name = "addProductName";
			this.addProductName.Size = new System.Drawing.Size(95, 22);
			this.addProductName.TabIndex = 14;
			// 
			// addProductPrice
			// 
			this.addProductPrice.Location = new System.Drawing.Point(342, 122);
			this.addProductPrice.Name = "addProductPrice";
			this.addProductPrice.Size = new System.Drawing.Size(95, 22);
			this.addProductPrice.TabIndex = 15;
			// 
			// addProductQty
			// 
			this.addProductQty.Location = new System.Drawing.Point(465, 122);
			this.addProductQty.Name = "addProductQty";
			this.addProductQty.Size = new System.Drawing.Size(95, 22);
			this.addProductQty.TabIndex = 16;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(972, 80);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(83, 17);
			this.label7.TabIndex = 17;
			this.label7.Text = "In Stock?";
			// 
			// searchByName
			// 
			this.searchByName.Location = new System.Drawing.Point(206, 237);
			this.searchByName.Name = "searchByName";
			this.searchByName.Size = new System.Drawing.Size(95, 22);
			this.searchByName.TabIndex = 18;
			// 
			// addInStock
			// 
			this.addInStock.Location = new System.Drawing.Point(957, 124);
			this.addInStock.Name = "addInStock";
			this.addInStock.Size = new System.Drawing.Size(95, 22);
			this.addInStock.TabIndex = 19;
			// 
			// addProductVendor
			// 
			this.addProductVendor.Location = new System.Drawing.Point(837, 124);
			this.addProductVendor.Name = "addProductVendor";
			this.addProductVendor.Size = new System.Drawing.Size(95, 22);
			this.addProductVendor.TabIndex = 20;
			// 
			// addProductCalories
			// 
			this.addProductCalories.Location = new System.Drawing.Point(721, 124);
			this.addProductCalories.Name = "addProductCalories";
			this.addProductCalories.Size = new System.Drawing.Size(95, 22);
			this.addProductCalories.TabIndex = 21;
			// 
			// addProductType
			// 
			this.addProductType.Location = new System.Drawing.Point(599, 124);
			this.addProductType.Name = "addProductType";
			this.addProductType.Size = new System.Drawing.Size(95, 22);
			this.addProductType.TabIndex = 22;
			// 
			// removeByName
			// 
			this.removeByName.Location = new System.Drawing.Point(206, 178);
			this.removeByName.Name = "removeByName";
			this.removeByName.Size = new System.Drawing.Size(95, 22);
			this.removeByName.TabIndex = 23;
			// 
			// editByName
			// 
			this.editByName.Location = new System.Drawing.Point(206, 297);
			this.editByName.Name = "editByName";
			this.editByName.Size = new System.Drawing.Size(95, 22);
			this.editByName.TabIndex = 24;
			// 
			// homePage
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1059, 525);
			this.Controls.Add(this.editByName);
			this.Controls.Add(this.removeByName);
			this.Controls.Add(this.addProductType);
			this.Controls.Add(this.addProductCalories);
			this.Controls.Add(this.addProductVendor);
			this.Controls.Add(this.addInStock);
			this.Controls.Add(this.searchByName);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.addProductQty);
			this.Controls.Add(this.addProductPrice);
			this.Controls.Add(this.addProductName);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.promptLabel);
			this.Controls.Add(this.exitButton);
			this.Controls.Add(this.reorderButton);
			this.Controls.Add(this.viewButton);
			this.Controls.Add(this.editButton);
			this.Controls.Add(this.searchButton);
			this.Controls.Add(this.removeButton);
			this.Controls.Add(this.addButton);
			this.Name = "homePage";
			this.Text = "Home Page";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button addButton;
		private System.Windows.Forms.Button removeButton;
		private System.Windows.Forms.Button searchButton;
		private System.Windows.Forms.Button editButton;
		private System.Windows.Forms.Button viewButton;
		private System.Windows.Forms.Button reorderButton;
		private System.Windows.Forms.Button exitButton;
		private System.Windows.Forms.Label promptLabel;
		private System.IO.FileSystemWatcher fileSystemWatcher1;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox addProductType;
		private System.Windows.Forms.TextBox addProductCalories;
		private System.Windows.Forms.TextBox addProductVendor;
		private System.Windows.Forms.TextBox addInStock;
		private System.Windows.Forms.TextBox searchByName;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox addProductQty;
		private System.Windows.Forms.TextBox addProductPrice;
		private System.Windows.Forms.TextBox addProductName;
		private System.Windows.Forms.TextBox editByName;
		private System.Windows.Forms.TextBox removeByName;
	}
}

