﻿namespace Exercise_1
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.submitButton = new System.Windows.Forms.Button();
			this.maleRadioButton = new System.Windows.Forms.RadioButton();
			this.promptLabel = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.firstNameLabel = new System.Windows.Forms.Label();
			this.lastNameLabel = new System.Windows.Forms.Label();
			this.passwordLabel = new System.Windows.Forms.Label();
			this.confirmPasswordLabel = new System.Windows.Forms.Label();
			this.emailAddressLabel = new System.Windows.Forms.Label();
			this.genderLabel = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.femaleRadioButton = new System.Windows.Forms.RadioButton();
			this.noAnswerRadioButton = new System.Windows.Forms.RadioButton();
			this.SuspendLayout();
			// 
			// submitButton
			// 
			this.submitButton.AutoSize = true;
			this.submitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.submitButton.Location = new System.Drawing.Point(177, 390);
			this.submitButton.Name = "submitButton";
			this.submitButton.Size = new System.Drawing.Size(114, 30);
			this.submitButton.TabIndex = 0;
			this.submitButton.Text = "Submit";
			this.submitButton.UseVisualStyleBackColor = true;
			// 
			// maleRadioButton
			// 
			this.maleRadioButton.AutoSize = true;
			this.maleRadioButton.Location = new System.Drawing.Point(231, 295);
			this.maleRadioButton.Name = "maleRadioButton";
			this.maleRadioButton.Size = new System.Drawing.Size(59, 21);
			this.maleRadioButton.TabIndex = 1;
			this.maleRadioButton.TabStop = true;
			this.maleRadioButton.Text = "Male";
			this.maleRadioButton.UseVisualStyleBackColor = true;
			// 
			// promptLabel
			// 
			this.promptLabel.AutoSize = true;
			this.promptLabel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.promptLabel.ForeColor = System.Drawing.SystemColors.MenuHighlight;
			this.promptLabel.Location = new System.Drawing.Point(89, 35);
			this.promptLabel.Name = "promptLabel";
			this.promptLabel.Size = new System.Drawing.Size(255, 23);
			this.promptLabel.TabIndex = 2;
			this.promptLabel.Text = "Please fill out the form below";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(231, 84);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(174, 22);
			this.textBox1.TabIndex = 3;
			// 
			// firstNameLabel
			// 
			this.firstNameLabel.AutoSize = true;
			this.firstNameLabel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.firstNameLabel.Location = new System.Drawing.Point(75, 84);
			this.firstNameLabel.Name = "firstNameLabel";
			this.firstNameLabel.Size = new System.Drawing.Size(110, 23);
			this.firstNameLabel.TabIndex = 4;
			this.firstNameLabel.Text = "First Name:";
			// 
			// lastNameLabel
			// 
			this.lastNameLabel.AutoSize = true;
			this.lastNameLabel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lastNameLabel.Location = new System.Drawing.Point(76, 123);
			this.lastNameLabel.Name = "lastNameLabel";
			this.lastNameLabel.Size = new System.Drawing.Size(109, 23);
			this.lastNameLabel.TabIndex = 5;
			this.lastNameLabel.Text = "Last Name:";
			// 
			// passwordLabel
			// 
			this.passwordLabel.AutoSize = true;
			this.passwordLabel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.passwordLabel.Location = new System.Drawing.Point(24, 165);
			this.passwordLabel.Name = "passwordLabel";
			this.passwordLabel.Size = new System.Drawing.Size(161, 23);
			this.passwordLabel.TabIndex = 6;
			this.passwordLabel.Text = "Create Password:";
			// 
			// confirmPasswordLabel
			// 
			this.confirmPasswordLabel.AutoSize = true;
			this.confirmPasswordLabel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.confirmPasswordLabel.Location = new System.Drawing.Point(14, 206);
			this.confirmPasswordLabel.Name = "confirmPasswordLabel";
			this.confirmPasswordLabel.Size = new System.Drawing.Size(171, 23);
			this.confirmPasswordLabel.TabIndex = 7;
			this.confirmPasswordLabel.Text = "Confirm Password:";
			// 
			// emailAddressLabel
			// 
			this.emailAddressLabel.AutoSize = true;
			this.emailAddressLabel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.emailAddressLabel.Location = new System.Drawing.Point(48, 247);
			this.emailAddressLabel.Name = "emailAddressLabel";
			this.emailAddressLabel.Size = new System.Drawing.Size(137, 23);
			this.emailAddressLabel.TabIndex = 8;
			this.emailAddressLabel.Text = "Email Address:";
			// 
			// genderLabel
			// 
			this.genderLabel.AutoSize = true;
			this.genderLabel.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.genderLabel.Location = new System.Drawing.Point(104, 295);
			this.genderLabel.Name = "genderLabel";
			this.genderLabel.Size = new System.Drawing.Size(81, 23);
			this.genderLabel.TabIndex = 9;
			this.genderLabel.Text = "Gender:";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(231, 206);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(174, 22);
			this.textBox2.TabIndex = 10;
			// 
			// textBox4
			// 
			this.textBox4.Location = new System.Drawing.Point(231, 247);
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(174, 22);
			this.textBox4.TabIndex = 12;
			// 
			// textBox5
			// 
			this.textBox5.Location = new System.Drawing.Point(231, 165);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(174, 22);
			this.textBox5.TabIndex = 13;
			// 
			// textBox6
			// 
			this.textBox6.Location = new System.Drawing.Point(231, 123);
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new System.Drawing.Size(174, 22);
			this.textBox6.TabIndex = 14;
			// 
			// femaleRadioButton
			// 
			this.femaleRadioButton.AutoSize = true;
			this.femaleRadioButton.Location = new System.Drawing.Point(330, 295);
			this.femaleRadioButton.Name = "femaleRadioButton";
			this.femaleRadioButton.Size = new System.Drawing.Size(75, 21);
			this.femaleRadioButton.TabIndex = 15;
			this.femaleRadioButton.TabStop = true;
			this.femaleRadioButton.Text = "Female";
			this.femaleRadioButton.UseVisualStyleBackColor = true;
			// 
			// noAnswerRadioButton
			// 
			this.noAnswerRadioButton.AutoSize = true;
			this.noAnswerRadioButton.Location = new System.Drawing.Point(231, 333);
			this.noAnswerRadioButton.Name = "noAnswerRadioButton";
			this.noAnswerRadioButton.Size = new System.Drawing.Size(157, 21);
			this.noAnswerRadioButton.TabIndex = 16;
			this.noAnswerRadioButton.TabStop = true;
			this.noAnswerRadioButton.Text = "Prefer not to answer";
			this.noAnswerRadioButton.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(483, 450);
			this.Controls.Add(this.noAnswerRadioButton);
			this.Controls.Add(this.femaleRadioButton);
			this.Controls.Add(this.textBox6);
			this.Controls.Add(this.textBox5);
			this.Controls.Add(this.textBox4);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.genderLabel);
			this.Controls.Add(this.emailAddressLabel);
			this.Controls.Add(this.confirmPasswordLabel);
			this.Controls.Add(this.passwordLabel);
			this.Controls.Add(this.lastNameLabel);
			this.Controls.Add(this.firstNameLabel);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.promptLabel);
			this.Controls.Add(this.maleRadioButton);
			this.Controls.Add(this.submitButton);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Name = "Form1";
			this.Text = "Registration Form";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button submitButton;
		private System.Windows.Forms.RadioButton maleRadioButton;
		private System.Windows.Forms.Label promptLabel;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label firstNameLabel;
		private System.Windows.Forms.Label lastNameLabel;
		private System.Windows.Forms.Label passwordLabel;
		private System.Windows.Forms.Label confirmPasswordLabel;
		private System.Windows.Forms.Label emailAddressLabel;
		private System.Windows.Forms.Label genderLabel;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.TextBox textBox6;
		private System.Windows.Forms.RadioButton femaleRadioButton;
		private System.Windows.Forms.RadioButton noAnswerRadioButton;
	}
}

